[![Built with Grunt](https://cdn.gruntjs.com/builtwith.png)](http://gruntjs.com/)
[![Build Status](https://api.shippable.com/projects/54142353f82ab7ebd69c9f61/badge?branchName=master&no=1424028796098)](https://api.shippable.com/projects/54142353f82ab7ebd69c9f61/badge?branchName=master&no=1424028796098)
# README #
This project is organized in 2 main applications sharing a Common set of modules. All communications are made using **JSON** as interface messaging and are secured using **JSON Web Tokens** encoded in [RS256 / HS512](http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html).
# Architecture #
## Overview ##
![WebGAP-HighLevel-OP.png](https://bytebucket.org/mcmartins/webgap/raw/2facd633ae8edd936fe0dbc864af82b2d1a7c52b/webgap-etc/images/WebGAP-HighLevel-OP.png)
## Communications ##
![Comunications.png](https://bytebucket.org/mcmartins/webgap/raw/2facd633ae8edd936fe0dbc864af82b2d1a7c52b/webgap-etc/images/Comunications.png)
