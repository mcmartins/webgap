/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 23-05-2014
 *
 */

var assert = require("assert"),
    Cache = require('../cache').Cache,
    cache;


describe('common.cache', function () {

    before(function (done) {
        cache = new Cache(({cacheSize: 5}));
        done();
    });

    it('should save a non existing key and return that value', function (done) {
        cache.set('one', 1, function (err, value) {
            assert.equal(value, 1);
            cache.size(function (err, size) {
                assert.equal(size, 1);
                done();
            });
        });
    });

    it('should not save an existing key', function (done) {
        cache.set('one', 1, function (err, value) {
            assert.equal(value, 1);
            cache.size(function (err, size) {
                assert.equal(size, 1);
                done();
            });
        });
    });

    it('should contain only one element in the store', function (done) {
        cache.size(function (err, size) {
            assert.equal(size, 1);
            done();
        });
    });

    it('should fill all the cache store', function (done) {
        cache.set('two', 2, function (err, value) {
            assert.equal(value, 2);
            cache.set('three', 3, function (err, value) {
                assert.equal(value, 3);
                cache.set('four', 4, function (err, value) {
                    assert.equal(value, 4);
                    cache.set('five', 5, function (err, value) {
                        assert.equal(value, 5);
                        done();
                    });
                });
            });
        });
    });

    it('should contain 5 entries', function (done) {
        cache.size(function (err, size) {
            assert.equal(size, 5);
            done();
        });
    });

});