/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 23-05-2014
 *
 */

var moment = require('moment');

/**
 * Memory cache module
 *
 * @param options {cacheSize, timeToLive}
 * @returns {Cache}
 * @constructor
 */
function Cache(options) {
    var self = this;
    // this eliminates the need to use 'new' keyword when instantiating the module
    if (!(self instanceof Cache)) {
        return new Cache(options);
    }
    // make options is optional
    options = options || {};
    // default cache size
    self.cacheSize = options.cacheSize || 50; // number of items
    // cache expires
    self.expire = options.expire || true; // cache expiration active
    // default expire threshold
    self.timeToLive = options.timeToLive || 86400000; // milliseconds
    // initialize the store object
    self.store = {};
}

/**
 * Add value to cache
 *
 * @param key the key of the value
 * @param value the value to store
 * @param callback
 * @returns {*} returns the value
 */
Cache.prototype.set = function (key, value, callback) {
    var self = this;
    // save value if key not present
    self.get(key, function (err, cachedValue) {
        // clear store if it exceeds the limit
        self.size(function (err, size) {
            if (size > self.cacheSize) {
                self.clear(function () {
                });
            }
        });
        // add value if not already defined
        if (!cachedValue) {
            self.store[key] = value;
            // add a expire timeout to remove the value from the store
            addExpire(self, key, function (err) {
            });
        }
        return callback(null, value);
    });
};

function addExpire(self, key, callback) {
    // only if cache expiration is active
    if (self.expire) {
        // add a timeout to clean each value in the cache
        var expire = moment().add(self.timeToLive, 'milliseconds');
        if (!isNaN(expire)) {
            setTimeout(function () {
                self.del(key, function (err) {
                });
            }, expire);
        }
    }
    return callback(null);
}

/**
 * Gets the value of the key, if exists otherwise null
 *
 * @param key the key
 * @param callback
 * @returns {*} the value for the requested key
 */
Cache.prototype.get = function (key, callback) {
    var cachedValue = this.store[key];
    return callback(null, cachedValue);
};

/**
 * Deletes a value from the cache
 *
 * @param key the key of the value to delete
 * @param callback
 */
Cache.prototype.del = function (key, callback) {
    var self = this;
    self.get(key, function (err, cachedValue) {
        if (cachedValue) {
            delete self.store[key];
        }
    });
    return callback(null);
};

/**
 * Gets the number of elements in cache
 *
 * @param callback
 * @returns {*}
 */
Cache.prototype.size = function (callback) {
    var length = Object.keys(this.store).length;
    return callback(null, length);
};

/**
 * Clear store
 *
 * @param callback
 * @returns {*}
 */
Cache.prototype.clear = function (callback) {
    this.store = {};
    return callback(null);
};

module.exports.Cache = Cache;