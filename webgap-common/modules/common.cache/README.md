# README #

This is a simple **in memory** caching system.

# Usage #

    var Cache = require('common.cache');
    Cache.set('Key', 'Value');
    console.log(Cache.get('Key'));
    Cache.del('Key');
    console.log(Cache.size());
    Cache.clear();
    console.log(Cache.size())

# JS Doc #

    /**
    * Memory cache module
    *
    * @options {size: number, timeToLive: number}
    * @type {{size: number, timeToLive: number, set: set, get: get, del: del, size: size, clear: clear}}
    */
    function Cache(options)

    /**
     * Add value to cache
     *
     * @param key the key of the value
     * @param value the value to store
     * @returns {*} returns the value
     */
    Cache.prototype.set = function (key, value)

    /**
     * Gets the value of the key, if exists otherwise null
     *
     * @param key the key
     * @returns {*} the value for the requested key
     */
    Cache.prototype.get = function (key)

    /**
     * Deletes a value from the cache
     *
     * @param key the key of the value to delete
    */
    Cache.prototype.del = function (key)

    /**
     * Gets the number of elements in cache
     *
     * @returns {number} the size of the cache (number of elements)
     */
    Cache.prototype.size = function ()

    /**
     * Clear all cache
    */
    Cache.prototype.clear = function ()
