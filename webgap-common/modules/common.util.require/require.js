/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 05-08-2014
 *
 */

var Finder = require('fs-finder');

/**
 * Imports automatically modules from a specific directory with a specific name
 *
 * @returns {AutomaticRequireUtils}
 * @constructor
 */
function AutomaticRequireUtils() {
    var self = this;
    // this eliminates the need to use 'new' keyword when instantiating the module
    if (!(self instanceof AutomaticRequireUtils)) {
        return new AutomaticRequireUtils();
    }
}

/**
 * Imports modules based on patterns
 *
 * @param root the root directory
 * @param pattern the pattern to search for
 * @param args the arguments for the require function
 */
AutomaticRequireUtils.prototype.require = function (root, pattern, args) {
    // find modules with 'pattern' in 'root' directory and 'require' them
    Finder.in(root).findDirectories(pattern).forEach(function (name) {
        require(name)(args);
    });
};

module.exports.AutomaticRequireUtils = AutomaticRequireUtils;