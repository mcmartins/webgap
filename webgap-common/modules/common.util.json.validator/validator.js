/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

var JaySchema = require('jayschema'),
    jsonValidator = new JaySchema(JaySchema.loaders.http);

/**
 * JSON Schemas validator
 *
 * @returns {JSONValidator}
 * @constructor
 */
function JSONValidator() {
    var self = this;
    // this eliminates the need to use 'new' keyword when instantiating the module
    if (!(self instanceof JSONValidator)) {
        return new JSONValidator();
    }
}

/**
 * Check if a json object is valid against a json schema
 *
 * @param data
 * @param callback
 */
//FIXME this does not work
JSONValidator.prototype.isValid = function (data, callback) {
    var schemaURL = data.render ? data.render.schema : undefined;
    this.downloadJSON(data, schemaURL, function (schema) {
        jsonValidator.validate(schema, callback);
    });
};

module.exports.JSONValidator = JSONValidator;
