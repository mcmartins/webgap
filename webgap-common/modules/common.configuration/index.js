/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

var convict = require('convict');

/**
 * Configurations
 *
 * @type {{init: Function}}
 */
module.exports = {

  load: function () {
    // require caches json files
    var schema = require('./configurations-schema.json');
    var configuration = require(process.env.NODE_APP_HOME + '/conf/' + process.env.NODE_APP + '-' + process.env.NODE_ENV + '.json');
    var conf = convict(schema).load(configuration);
    // validate the configurations
    conf.validate();
    return conf;
  }
};