{
  "SERVER": {
    "IP": {
      "doc": "The Server IP address to bind.",
      "format": "ipaddress",
      "default": "0.0.0.0",
      "env": "SERVER_IP"
    },
    "HOST": {
      "doc": "The Server Host to bind.",
      "default": "portal.webgap.eu",
      "env": "SERVER_HOST"
    },
    "PORT": {
      "doc": "The Server Port to bind.",
      "format": "port",
      "default": 8080,
      "env": "SERVER_PORT"
    },
    "PROTOCOL": {
      "doc": "The Server Protocol to access.",
      "format": [
        "http://",
        "https://"
      ],
      "default": "https://",
      "env": "SERVER_PROTOCOL"
    },
    "APACHE_PORT": {
      "doc": "The Apache Server Port to bind.",
      "format": "port",
      "default": 80,
      "env": "APACHE_PORT"
    },
    "WEBSOCKETS_PORT": {
      "doc": "The WebSocket Port to bind.",
      "format": "port",
      "default": 8000,
      "env": "SERVER_WEBSOCKETS_PORT"
    },
    "CERTIFICATES_PATH": {
      "doc": "The certificates path.",
      "default": "wrong_path",
      "env": "SERVER_CERTIFICATES_PATH"
    }
  },
  "DB": {
    "DRIVER": {
      "doc": "The database driver to use.",
      "format": [
        "mongodb",
        "jsonfs"
      ],
      "default": "jsonfs",
      "env": "DB_DRIVER"
    },
    "CONNECTION_STRING": {
      "doc": "The database connection string.",
      "default": "~",
      "env": "DB_CONNECTION_STRING"
    }
  },
  "MODULE": {
    "ACCOUNT": {
      "USER_MAX_RAM": {
        "doc": "The default maximum amount of RAM per user per instance (Bytes).",
        "format": "int",
        "default": 268435456,
        "env": "MODULE_ACCOUNT_USER_MAX_RAM"
      },
      "USER_MAX_SPACE": {
        "doc": "The default maximum amount of Disk Space per user per instance (Bytes).",
        "format": "int",
        "default": 268435456,
        "env": "MODULE_ACCOUNT_USER_MAX_SPACE"
      },
      "USER_MAX_APPS": {
        "doc": "The default maximum number of apps.",
        "format": "int",
        "default": 10,
        "env": "MODULE_ACCOUNT_USER_MAX_APPS"
      },
      "USER_MAX_SLOTS": {
        "doc": "The default maximum simultaneous orders.",
        "format": "int",
        "default": 10,
        "env": "MODULE_ACCOUNT_USER_MAX_SLOTS"
      }
    },
    "AUTHENTICATION": {
      "GOOGLE": {
        "CLIENT_SECRET": {
          "doc": "The Google Api Secret.",
          "default": "invalid_key",
          "env": "GOOGLE_CLIENT_SECRET"
        },
        "CLIENT_ID": {
          "doc": "The Google Api Client ID.",
          "default": "invalid_key",
          "env": "GOOGLE_CLIENT_ID"
        }
      },
      "DROPBOX": {
        "CLIENT_SECRET": {
          "doc": "The Dropbox Api Secret.",
          "default": "invalid_key",
          "env": "DROPBOX_CLIENT_SECRET"
        },
        "CLIENT_ID": {
          "doc": "The Dropbox Api Client ID.",
          "default": "invalid_key",
          "env": "DROPBOX_CLIENT_ID"
        }
      },
      "TWITTER": {
        "CLIENT_SECRET": {
          "doc": "The Twitter Api Secret.",
          "default": "invalid_key",
          "env": "TWITTER_CLIENT_SECRET"
        },
        "CLIENT_ID": {
          "doc": "The Twitter Api Client ID.",
          "default": "invalid_key",
          "env": "TWITTER_CLIENT_ID"
        }
      },
      "GITHUB": {
        "CLIENT_SECRET": {
          "doc": "The Github Api Secret.",
          "default": "invalid_key",
          "env": "GITHUB_CLIENT_SECRET"
        },
        "CLIENT_ID": {
          "doc": "The Github Api Client ID.",
          "default": "invalid_key",
          "env": "GITHUB_CLIENT_ID"
        }
      },
      "BITBUCKET": {
        "CLIENT_SECRET": {
          "doc": "The Bitbucket Api Secret.",
          "default": "invalid_key",
          "env": "BITBUCKET_CLIENT_SECRET"
        },
        "CLIENT_ID": {
          "doc": "The Bitbucket Api Client ID.",
          "default": "invalid_key",
          "env": "BITBUCKET_CLIENT_ID"
        }
      },
      "MICROSOFT": {
        "CLIENT_SECRET": {
          "doc": "The Microsoft Api Secret.",
          "default": "invalid_key",
          "env": "MICROSOFT_CLIENT_SECRET"
        },
        "CLIENT_ID": {
          "doc": "The Microsoft Api Client ID.",
          "default": "invalid_key",
          "env": "MICROSOFT_CLIENT_ID"
        }
      }
    }
  },
  "AMQP": {
    "IP": {
      "doc": "The Topic Server IP address to bind.",
      "format": "ipaddress",
      "default": "127.0.0.1",
      "env": "AMQP_IP"
    },
    "HOST": {
      "doc": "The Topic Server Host to bind.",
      "default": "webgap.eu",
      "env": "AMQP_HOST"
    },
    "PORT": {
      "doc": "The Topic Server Port to bind.",
      "format": "port",
      "default": 8080,
      "env": "AMQP_PORT"
    },
    "RETRY": {
      "doc": "The Topic retry in (milliseconds).",
      "format": "int",
      "default": 5000,
      "env": "AMQP_RETRY"
    },
    "CHANNELS": {
      "RESPONSE": {
        "doc": "The Topic response Channel.",
        "default": "/services/response",
        "env": "AMQP_CHANNELS_RESPONSE"
      },
      "REQUEST": {
        "doc": "The Topic request Channel.",
        "default": "/services/request",
        "env": "AMQP_CHANNELS_REQUEST"
      },
      "STATUS": {
        "doc": "The Topic for services status.",
        "default": "/services/status",
        "env": "AMQP_CHANNELS_STATUS"
      }
    }
  },
  "GENERAL": {
    "LOG": {
      "FILE": {
        "doc": "The path to the application log file.",
        "default": "/tmp/application.log",
        "env": "LOG_FILE"
      },
      "EXCEPTIONS_FILE": {
        "doc": "The path to the application exceptions log file.",
        "default": "/tmp/application-exceptions.log",
        "env": "LOG_EXCEPTIONS_FILE"
      },
      "LEVEL": {
        "doc": "The logging level.",
        "format": [
          "debug",
          "info",
          "warn",
          "error"
        ],
        "default": "info",
        "env": "LOG_LEVEL"
      }
    },
    "COOKIE": {
      "SECRET": {
        "doc": "The Cookie Secret key.",
        "default": "invalid_key",
        "env": "COOKIE_SECRET"
      },
      "MAX_AGE": {
        "doc": "The Cookie maximum age (milliseconds).",
        "format": "int",
        "default": 3600000,
        "env": "COOKIE_MAX_AGE"
      }
    },
    "TOKEN": {
      "SECRET": {
        "doc": "The Token Secret key.",
        "default": "invalid_key",
        "env": "TOKEN_SECRET"
      },
      "CERTIFICATES_PATH": {
        "doc": "The certificates path.",
        "default": "wrong_path",
        "env": "TOKEN_CERTIFICATES_PATH"
      },
      "EXPIRING_TIME_THRESHOLD": {
        "doc": "The Token expiring threshold (milliseconds).",
        "format": "int",
        "default": 5000,
        "env": "TOKEN_EXPIRING_TIME_THRESHOLD"
      },
      "VALIDITY_TIME": {
        "doc": "The Token validity time (milliseconds).",
        "format": "int",
        "default": 5000,
        "env": "TOKEN_VALIDITY_TIME"
      }
    },
    "DATE": {
      "TIMEZONE": {
        "doc": "The Date timezone.",
        "default": "UTC",
        "env": "TIMEZONE"
      },
      "DATE_FORMAT": {
        "doc": "The Date Format.",
        "default": "YYYY-MM-DD Z",
        "env": "DATE_FORMAT"
      },
      "TIME_FORMAT": {
        "doc": "The Time format.",
        "default": "HH:mm:ss Z",
        "env": "TIME_FORMAT"
      },
      "DATETIME_FORMAT": {
        "doc": "The DateTime format.",
        "default": "YYYY-MM-DDTHH:mm:ss Z",
        "env": "DATETIME_FORMAT"
      }
    },
    "TIMEOUT": {
      "NORMAL": {
        "doc": "The Default Request Timeout (milliseconds).",
        "format": "int",
        "default": 500,
        "env": "NORMAL_TIMEOUT"
      },
      "OPTIMIST": {
        "doc": "The Optimistic Request Timeout (milliseconds).",
        "format": "int",
        "default": 250,
        "env": "OPTIMIST_TIMEOUT"
      },
      "PESSIMIST": {
        "doc": "The Pessimistic Request Timeout (milliseconds).",
        "format": "int",
        "default": 100,
        "env": "PESSIMIST_TIMEOUT"
      }
    }
  }
}