/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 12-08-2014
 *
 */

var amqp = require('amqp');

/**
 * AMQP Client module
 *
 * @returns {Client}
 * @constructor
 */
function Client() {
    // this eliminates the need to use 'new' keyword when instantiating the module
    if (!(this instanceof Client)) {
        return new Client();
    }
}

/**
 * Initialise method
 *
 * @param options
 * @param callback
 * @returns {*}
 */
Client.prototype.connect = function (options, callback) {
    var self = this;
    // if someone passes just the callback
    if (typeof options === 'function') {
        callback = options;
    } else {
        // override options
        self.options = options || {};
    }
    // TODO options should contain all configurations necessary
    var clientOptions = {
        host: self.options.configuration.get('AMQP.HOST'),
        port: self.options.configuration.get('AMQP.PORT'),
        login: 'guest',
        password: '6rLvkViJF7NJ',
        authMechanism: 'AMQPLAIN',
        vhost: '/',
        clientProperties: {
            applicationName: process.env.NODE_APP,
            capabilities: {
                consumer_cancel_notify: true
            }
        }
        //ssl: {
        //    enabled: true,
        //    keyFile: utils.configuration.get("SERVER.CERTIFICATES_PATH") + '/ssl.key',
        //    certFile: utils.configuration.get("SERVER.CERTIFICATES_PATH") + '/ssl.crt',
        //    //caFile: '/path/to/cacert/file',
        //    rejectUnauthorized: true
        //}
    };

    var AMQPOptions = {
        defaultExchangeName: '',
        reconnect: true,
        reconnectBackoffStrategy: 'linear',
        reconnectExponentialLimit: 120000,
        reconnectBackoffTime: self.options.configuration.get('AMQP.RETRY')
    };

    this.options.logger.log("info", "initializing AMQP Client...");

    var client = amqp.createConnection(clientOptions, AMQPOptions);
    // Wait for connection to become established.
    client.on('ready', function () {
        self.options.logger.log("info", "AMQP Client ready!");
    });

    return callback(null, client);
};

module.exports = Client;