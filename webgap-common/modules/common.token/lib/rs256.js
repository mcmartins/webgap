/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 23-05-2014
 *
 */

var AbstractAlgorithm = require('./abstract.js').AbstractAlgorithm,
    fs = require('fs'),
    util = require('util');

/**
 * RS256 Algorithm implementation
 *
 * @param {Object} options
 *        {String} options.privateKeyPath
 *        {String} options.publicKeyPath
 * @constructor
 */
function RS256Algorithm(options) {
    RS256Algorithm.super_.apply(this, arguments);

    var self = this;
    options = options || {};
    // this eliminates the need to use 'new' keyword when instantiating the module
    if (!(self instanceof RS256Algorithm)) {
        return new RS256Algorithm(options);
    }
    // algorithm
    self.ALGORITHM = 'RS256';
    // load certificates
    if (!options.privateKeyPath || !options.publicKeyPath) {
        console.error('Parameter \'options.privateKeyPath\' or \'options.publicKeyPath\' are missing.');
        throw new Error('Parameter \'options.privateKeyPath\' or \'options.publicKeyPath\' are missing.');
    }
    self.pem = fs.readFileSync(options.privateKeyPath).toString('ascii');
    self.cert = fs.readFileSync(options.publicKeyPath).toString('ascii');
}

/* inherit AbstractAlgorithm Module */
util.inherits(RS256Algorithm, AbstractAlgorithm);

/**
 * Encodes an object using RS256
 *
 * @param data the data to encode
 * @returns {*}
 */
RS256Algorithm.prototype.encode = function (data) {
    return this.jwt.encode(data, this.pem, this.ALGORITHM);
};

/**
 * Decodes a specific token encoded in RS256
 *
 * @param token the token to decode
 * @returns {*}
 */
RS256Algorithm.prototype.decode = function (token) {
    var decodedToken = null;
    try {
        decodedToken = this.jwt.decode(token, this.cert);
    } catch (error) {
        console.log("Could not decode token! " + error);
        decodedToken = false;
    }
    return decodedToken;
};

module.exports.RS256Algorithm = RS256Algorithm;