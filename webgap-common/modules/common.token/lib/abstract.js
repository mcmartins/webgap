/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 23-05-2014
 *
 */

var moment = require('moment'),
    jwt = require('jwt-simple');

/**
 * Abstract Token utilities
 *
 * @returns {AbstractAlgorithm}
 * @constructor
 */
function AbstractAlgorithm() {
    var self = this;
    // this eliminates the need to use 'new' keyword when instantiating the module
    if (!(self instanceof AbstractAlgorithm)) {
        return new AbstractAlgorithm();
    }
    // imports
    this.jwt = jwt;
}

/**
 * Encodes an object using HS512
 *
 * @param data the data to encode
 * @returns {*}
 */
AbstractAlgorithm.prototype.encode = function (data) {
    throw new Error('Not implemented');
};

/**
 * Decodes a specific token encoded in HS512
 *
 * @param token the token to decode
 * @returns {*}
 */
AbstractAlgorithm.prototype.decode = function (token) {
    new Error('Not implemented');
};

/**
 * Check whether a token is valid by check is integrity and validity
 *
 * @param token the token to validate
 * @returns {*}
 */
AbstractAlgorithm.prototype.isValid = function (token) {
    var decodedToken = this.decode(token);
    return decodedToken && moment().isBefore(moment(decodedToken.expiresAt));
};

module.exports.AbstractAlgorithm = AbstractAlgorithm;