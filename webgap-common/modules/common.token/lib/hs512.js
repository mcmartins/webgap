/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 23-05-2014
 *
 */

var AbstractAlgorithm = require('./abstract.js').AbstractAlgorithm,
    util = require('util');

/**
 * Token utilities
 *
 * @param {Object} options
 *        {String} options.secret
 * @constructor
 */
function HS512Algorithm(options) {
    HS512Algorithm.super_.apply(this, arguments);

    var self = this;
    options = options || {};
    // this eliminates the need to use 'new' keyword when instantiating the module
    if (!(self instanceof HS512Algorithm)) {
        return new HS512Algorithm(options);
    }
    // algorithm
    self.ALGORITHM = 'HS512';
    if (!options.secret) {
        console.error('Parameter \'options.secret\' is missing.');
        throw new Error('Parameter \'options.secret\' is missing.');
    }
    self.secret = options.secret;
}

/* inherit AbstractAlgorithm Module */
util.inherits(HS512Algorithm, AbstractAlgorithm);

/**
 * Encodes an object using HS512
 *
 * @param data the data to encode
 * @returns {*}
 */
HS512Algorithm.prototype.encode = function (data) {
    return this.jwt.encode(data, this.secret, this.ALGORITHM);
};

/**
 * Decodes a specific token encoded in HS512
 *
 * @param token the token to decode
 * @returns {*}
 */
HS512Algorithm.prototype.decode = function (token) {
    var decodedToken = null;
    try {
        decodedToken = this.jwt.decode(token, this.secret);
    } catch (error) {
        console.log("Could not decode token! " + error);
        decodedToken = false;
    }
    return decodedToken;
};

module.exports.HS512Algorithm = HS512Algorithm;