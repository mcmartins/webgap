/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

var configuration = require('../common.configuration').load(),
    message = require('../common.util.json.message');

/**
 * Route utility methods
 *
 * @returns {RouteUtils}
 * @constructor
 */
function RouteUtils() {
  var self = this;
  // this eliminates the need to use 'new' keyword when instantiating the module
  if (!(self instanceof RouteUtils)) {
    return new RouteUtils();
  }
}

/**
 * Adds a timeout on route handling. Used to prevent freeze requests
 *
 * @param timeout the timeout in milliseconds
 * @param res the session response
 * @param req the session request
 * @param callback the function to execute when timeout is fired
 */
RouteUtils.prototype.addTimeout = function (timeout, res, req, callback) {
  setTimeout(function () {
    // ignore timeout if resolution is finished
    if (!res.finished) {
      // TODO use common.notifier module
      req._passport.session.message = message.Client.generate(message.Client.Type.ERROR,
          "messages.error.timeout-request-title", "messages.error.timeout-request");
      res.status(408).redirect('/timeout');
    }
    //execute the callback
    callback();
  }, timeout || configuration.get('GENERAL.TIMEOUT.NORMAL_TIMEOUT'));
};

/**
 * Returns a json object for invalid requests
 *
 * @returns {*}
 */
RouteUtils.prototype.getNotFoundResponse = function () {
  var response = message.Server.Response();
  response.error.code = "404";
  response.error.details = "Oops! Cannot resolve it...Not a valid url!";
  return response;
};

module.exports.RouteUtils = RouteUtils;