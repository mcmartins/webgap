/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

var osUtils = require('osutils');

/**
 * Handles system memory statistics
 *
 * @returns {MemorySystemUtils}
 * @constructor
 */
function MemorySystemUtils() {
    var self = this;
    // this eliminates the need to use 'new' keyword when instantiating the module
    if (!(self instanceof MemorySystemUtils)) {
        return new MemorySystemUtils();
    }
}

/**
 * Returns the total available memory
 *
 * @param callback
 * @returns {*}
 */
MemorySystemUtils.prototype.getTotalMemory = function (callback) {
    return callback(osUtils.totalmem());
};

/**
 * Returns the total free memory
 *
 * @param callback
 * @returns {*}
 */
MemorySystemUtils.prototype.getFreeMemory = function (callback) {
    return callback(osUtils.freemem());
};

/**
 * Returns the free memory percentage
 *
 * @param callback
 * @returns {*}
 */
MemorySystemUtils.prototype.getFreeMemoryPercentage = function (callback) {
    return callback(osUtils.freememPercentage());
};

module.exports.MemorySystemUtils = MemorySystemUtils;