/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

var osUtils = require('os-utils');

/**
 * Handles system cpu statistics
 *
 * @returns {CPUSystemUtils}
 * @constructor
 */
function CPUSystemUtils() {
    var self = this;
    // this eliminates the need to use 'new' keyword when instantiating the module
    if (!(self instanceof CPUSystemUtils)) {
        return new CPUSystemUtils();
    }
}

/**
 * Returns the load average for specific time
 *
 * @param time one of 1, 5, 15
 * @param callback
 * @returns {*}
 */
CPUSystemUtils.prototype.getLoadAverage = function (time, callback) {
    return callback(osUtils.loadavg(time));
};

/**
 * Returns the number of CPU cores available
 *
 * @param callback
 * @returns {*}
 */
CPUSystemUtils.prototype.getNumberOfCPUs = function (callback) {
    return callback(osUtils.cpuCount());
};

/**
 * Returns the CPU usage
 *
 * @param callback
 */
CPUSystemUtils.prototype.getCPUUsage = function (callback) {
    osUtils.cpuUsage(callback);
};

/**
 * Returns the CPU free
 *
 * @param callback
 */
CPUSystemUtils.prototype.getCPUFree = function (callback) {
    osUtils.cpuFree(callback);
};

/**
 * Handles system cpu statistics
 *
 * @type {{getLoadAverage: getLoadAverage, getNumberOfCPUs: getNumberOfCPUs, getCPUUsage: getCPUUsage, getCPUFree: getCPUFree}}
 */
module.exports.CPUSystemUtils = CPUSystemUtils;