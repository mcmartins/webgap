/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 07-08-2014
 *
 */

var AbstractCloudClient = require('./lib/abstract.js').AbstractCloudClient,
    DropboxCloudClient = require('./lib/dropbox.js').DropboxCloudClient,
    MicrosoftCloudClient = require('./lib/windowslive.js').MicrosoftCloudClient,
    GoogleCloudClient = require('./lib/google.js').GoogleCloudClient,
    Logger = require('../common.util.logging').Logger,
    logger = new Logger();

/**
 * Cloud Data Access Object
 *
 * @returns {CloudDAO}
 * @constructor
 */
function CloudDAO() {
  var self = this;
  // this eliminates the need to use 'new' keyword when instantiating the module
  if (!(self instanceof CloudDAO)) {
    return new CloudDAO();
  }
  logger.info("Initializing Cloud Interface Manager...");
  // register default clients
  self.clients = {};
  self.register("dropbox", DropboxCloudClient);
  self.register("windowslive", MicrosoftCloudClient);
  self.register("google", GoogleCloudClient);
}

/**
 * Initialize Data Access Object using the authentication token
 *
 * @param identity a valid identity
 * @param callback
 * @returns {*}
 */
CloudDAO.prototype.getClient = function (identity, callback) {
  // resolve client for identity provider
  var Class = this.clients[identity.provider];
  // instantiate the class
  // a new class is created for each request due to the client token
  return callback(null, new Class(identity.token.accessToken));
};

/**
 * Register a new Cloud Storage System
 *
 * @param name the name of the client
 * @param Class a class that inherits from AbstractCloudClient
 * @throws Error if class doesn't inherits AbstractCloudClient
 */
CloudDAO.prototype.register = function (name, Class) {
  // check if class is an instance of AbstractCloudClient
  var instance = new Class();
  if (!(instance instanceof AbstractCloudClient)) {
    logger.error('Cloud Client is not a valid AbstractCloudClient.');
    throw new Error('Cloud Client is not a valid AbstractCloudClient.');
  }
  this.clients[name] = AbstractCloudClient;
};

module.exports.CloudDAO = CloudDAO;