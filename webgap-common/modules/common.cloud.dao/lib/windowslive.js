/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 07-08-2014
 *
 */

var AbstractCloudClient = require('./abstract.js').AbstractCloudClient,
    google = require('google-drive'),
    util = require('util');

/**
 * Dropbox Cloud client API
 *
 * @param accessToken the authentication token
 * @returns {MicrosoftCloudClient}
 * @constructor
 */
function MicrosoftCloudClient(accessToken) {
  MicrosoftCloudClient.super_.apply(this, arguments);

  var self = this;
  // this eliminates the need to use 'new' keyword when instantiating the module
  if (!(self instanceof MicrosoftCloudClient)) {
    return new MicrosoftCloudClient(accessToken);
  }
  // initialize the client
  this.client = new google.Client({
    key: self.configuration.get('MODULE.AUTHENTICATION.DROPBOX.CLIENT_ID'),
    secret: self.configuration.get('MODULE.AUTHENTICATION.DROPBOX.CLIENT_SECRET'),
    token: accessToken
  });
}

/* inherit Abstract Module */
util.inherits(MicrosoftCloudClient, AbstractCloudClient);

/**
 * {@inheritDoc}
 */
MicrosoftCloudClient.prototype.getAccountInfo = function (options, callback) {
  client.getAccountInfo(options, callback);
};

/**
 * {@inheritDoc}
 */
MicrosoftCloudClient.prototype.createDir = function (path, callback) {
  client.mkdir(path, callback);
};

/**
 * {@inheritDoc}
 */
MicrosoftCloudClient.prototype.readDir = function (path, callback) {
  client.readdir(path, callback);
};

/**
 * {@inheritDoc}
 */
MicrosoftCloudClient.prototype.writeFile = function (path, data, options, callback) {
  client.writeFile(path, data, options, callback);
};

/**
 * {@inheritDoc}
 */
MicrosoftCloudClient.prototype.readFile = function (path, options, callback) {
  client.readFile(path, options, callback);
};

/**
 * {@inheritDoc}
 */
MicrosoftCloudClient.prototype.share = function (path, options, callback) {
  client.makeUrl(path, options, callback);
};

module.exports.MicrosoftCloudClient = MicrosoftCloudClient;
