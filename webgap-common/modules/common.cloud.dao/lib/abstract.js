/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 07-08-2014
 *
 */

var configuration = require('../../common.configuration').load(),
    Logger = require('../../common.util.logging').Logger,
    logger = new Logger();

/**
 * Dropbox Cloud client API
 *
 * @param accessToken the authentication token
 * @returns {AbstractCloudClient}
 * @constructor
 */
function AbstractCloudClient(accessToken) {
  var self = this;
  // this eliminates the need to use 'new' keyword when instantiating the module
  if (!(self instanceof AbstractCloudClient)) {
    return new AbstractCloudClient(accessToken);
  }
  // initialize the client
  this.client = undefined;
  this.configuration = configuration;
}

/**
 * Returns the account information
 *
 * @param options
 * @param callback
 */
AbstractCloudClient.prototype.getAccountInfo = function (options, callback) {
  return callback(new Error('Not implemented'));
};

/**
 * Creates a directory
 *
 * @param path the directory path
 * @param callback
 */
AbstractCloudClient.prototype.createDir = function (path, callback) {
  return callback(new Error('Not implemented'));
};

/**
 * Reads the content of a directory
 *
 * @param path the directory path to read
 * @param callback
 */
AbstractCloudClient.prototype.readDir = function (path, callback) {
  return callback(new Error('Not implemented'));
};

/**
 * Writes a file in a specific path
 *
 * @param path the path to the file
 * @param data the data to write
 * @param options
 * @param callback
 */
AbstractCloudClient.prototype.writeFile = function (path, data, options, callback) {
  return callback(new Error('Not implemented'));
};

/**
 * Reads a File
 *
 * @param path the File path
 * @param options
 * @param callback
 */
AbstractCloudClient.prototype.readFile = function (path, options, callback) {
  return callback(new Error('Not implemented'));
};

/**
 * Returns a share link to a specified resource (File / Folder)
 *
 * @param path the File / Folder path
 * @param options
 * @param callback
 */
AbstractCloudClient.prototype.share = function (path, options, callback) {
  return callback(new Error('Not implemented'));
};

/**
 * Sync folder / file
 *
 * @param path the File / Folder path
 * @param options
 * @param callback
 */
AbstractCloudClient.prototype.sync = function (path, options, callback) {
  return callback(new Error('Not implemented'));
};

module.exports.AbstractCloudClient = AbstractCloudClient;
