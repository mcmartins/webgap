/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 05-05-2014
 *
 */

var Action = require('./../common.notifier/lib/action').Action,
    Audit = require('./lib/Audit').Audit,
    Notification = require('./../common.notifier/lib/notification').Notification,
    Identity = require('./lib/user/Identity').Identity,
    IdentityToken = require('./lib/user/IdentityToken').IdentityToken,
    UserPojo = require('./lib/user/UserPojo').UserPojo,
    SessionUserPojo = require('./lib/user/SessionUserPojo').SessionUserPojo,
    User = require('./lib/user/User').User;

/**
 * Expose constructors.
 */
module.exports.Action = Action;
module.exports.Audit = Audit;
module.exports.Notification = Notification;
module.exports.Identity = Identity;
module.exports.IdentityToken = IdentityToken;
module.exports.User = User;
module.exports.UserPojo = UserPojo;
module.exports.SessionUserPojo = SessionUserPojo;
