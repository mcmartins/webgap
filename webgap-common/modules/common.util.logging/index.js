/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 29-05-2014
 *
 */

var winston = require('winston'),
    configuration = require('../common.configuration').load();

/**
 * Initializes a logger based on winston
 *
 * @param {Object} options
 *        {String} options.applicationLogPath
 *        {Array} options.applicationExceptionsLogPath
 *        {Object} options.level
 * @returns {Logger}
 * @constructor
 */
function Logger(options) {
  var self = this;
  // this eliminates the need to use 'new' keyword when instantiating the module
  options = options || {};
  if (!(self instanceof Logger)) {
    return new Logger(options);
  }
  return new winston.Logger({
    transports: [
      new winston.transports.Console({
        json: false,
        timestamp: true,
        level: options.level || configuration.get('GENERAL.LOG.LEVEL')
      }),
      new winston.transports.File({
        json: false,
        timestamp: true,
        filename: options.applicationLogPath || configuration.get('GENERAL.LOG.FILE'),
        level: options.level || configuration.get('GENERAL.LOG.LEVEL')
      })
    ],
    exceptionHandlers: [
      new winston.transports.Console({
        json: false,
        timestamp: true,
        level: options.level || configuration.get('GENERAL.LOG.LEVEL')
      }),
      new winston.transports.File({
        json: false,
        timestamp: true,
        filename: options.applicationExceptionsLogPath || configuration.get('GENERAL.LOG.EXCEPTIONS_FILE'),
        level: options.level || configuration.get('GENERAL.LOG.LEVEL')
      })
    ],
    exitOnError: false
  });
}

module.exports.Logger = Logger;