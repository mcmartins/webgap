/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 07-08-2014
 *
 */

var Notification = require('../common.notifier').Notifier.Notification;

/**
 * Handles authorization for http requests
 *
 * @returns {Authorizator}
 * @constructor
 */
function Authorizator(utils) {
  var self = this;
  // this eliminates the need to use 'new' keyword when instantiating the module
  if (!(self instanceof Authorizator)) {
    return new Authorizator(utils);
  }
  this.utils = utils;
}

/**
 * Checks whether a user with a specific role is authorized or not
 *
 * @param roles
 * @returns {Function}
 */
Authorizator.prototype.isAuthorized = function (roles) {
  var self = this;
  return function (req, res, next) {
    // mandatory notifier POPUP options
    var options = {
      notification: "",
      request: req
    };
    if (!req._passport.session.user) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.WARNING,
        messageTitle: "messages.warning.authentication-required-title",
        messageText: "messages.warning.authentication-required",
        messageData: null
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.DONE;
      options.notification = new Notification(null, message, action, null);
      self.utils.notifier.notify(options, function (err) {
        if (err) {
          self.utils.logger.error('Something went wrong with the notification: ' + JSON.stringify(options.message) + "\n" + err);
        }
        res.redirect('/auth/login');
      });
    } else if (self.hasAccess(req.user, roles)) {
      next();
    } else {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.ERROR,
        messageTitle: "messages.error.authorization-required-title",
        messageText: "messages.error.authorization-required",
        messageData: null
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.DONE;
      options.notification = new Notification(null, message, action, null);
      self.utils.notifier.notify(options, function (err) {
        if (err) {
          self.utils.logger.error('Something went wrong with the notification: ' + JSON.stringify(options.message) + "\n" + err);
        }
        res.redirect('/unauthorized');
      });
    }
  }
};

/**
 * Returns true if an User is authorized to access
 *
 * @param user
 * @param roles
 * @returns {boolean}
 */
Authorizator.prototype.hasAccess = function (user, roles) {
  return roles.some(function (element) {
    return user.roles.indexOf(element) > -1
  });
};

module.exports.Authorizator = Authorizator;