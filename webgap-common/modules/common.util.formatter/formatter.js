/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 08-09-2014
 *
 */

var configuration = require('../common.configuration').load(),
    atma = require('atma-formatter'),
    moment = require('moment'),
    normat = require('normat'),
    time = normat.ms,
    size = normat.b;

/**
 * Formatter utility class
 *
 * @returns {Formatter}
 * @constructor
 */
function Formatter() {
  var self = this;
  // this eliminates the need to use 'new' keyword when instantiating the module
  if (!(self instanceof Formatter)) {
    return new Formatter();
  }
}

/**
 * Formats numeric values to *.00
 *
 * @param val the value to format
 * @returns {*|exports}
 */
Formatter.prototype.formatNumber = function (val) {
  return atma(val, ",0.00");
};

/**
 * Format numeric values to bytes
 *
 * @param val the value to format
 * @returns {*}
 */
Formatter.prototype.formatBytes = function (val) {
  return size(val);
};

/**
 * Format numeric values to seconds
 *
 * @param val the value to format
 * @returns {*}
 */
Formatter.prototype.formatSeconds = function (val) {
  return time(val);
};

/**
 * Format numeric values to percentage (does not calculate it)
 *
 * @param val the value to format
 * @returns {*}
 */
Formatter.prototype.formatPercentage = function (val) {
  return [this.formatNumber(val) + '%'].join();
};

/**
 * Format values to date format
 *
 * @param val the value to format
 * @returns {*}
 */
Formatter.prototype.formatDate = function (val) {
  return moment(val || new Date(), configuration.get('GENERAL.DATE.DATE_FORMAT'));
};

/**
 * Format values to datetime format
 *
 * @param val the value to format
 * @returns {*}
 */
Formatter.prototype.formatDateTime = function (val) {
  return moment(val || new Date(), configuration.get('GENERAL.DATE.DATETIME_FORMAT'));
};

/**
 * Format values to time format
 *
 * @param val the value to format
 * @returns {*}
 */
Formatter.prototype.formatTime = function (val) {
  return moment(val || new Date(), configuration.get('GENERAL.DATE.TIME_FORMAT'));
};

/**
 * Format values to from now representation
 *
 * @param val the value to format
 * @returns {*}
 */
Formatter.prototype.formatDateFromNow = function (val) {
  return moment(val || new Date()).fromNow();
};

module.exports.Formatter = Formatter;