/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 08-08-2014
 *
 */

var JSONValidator = require('../../common.util.json.validator/validator.js').JSONValidator,
    token = require('../../common.token/index.js'),
    Formatter = require('../../common.util.formatter/formatter.js').Formatter,
    configuration = require('../../common.configuration').load(),
    CommonNotifier = require('../../common.notifier'),
    Logger = require('../../common.util.logging').Logger,
    logger = new Logger();

/**
 * Basic Module
 * Contains the basic behaviour for a regular module
 *
 * @constructor
 */
function BaseModule() {

  logger.log("info", "initializing Base Module...");

  // create utils object
  this.utils = {
    notifier: new CommonNotifier.Notifier(),
    json: new JSONValidator(),
    tokenizer: token,
    logger: logger,
    formatter: new Formatter(),
    configuration: configuration
  };
}

module.exports.BaseModule = BaseModule;