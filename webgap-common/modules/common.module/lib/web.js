/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 08-08-2014
 *
 */

var fs = require('fs'),
    http = require('http'),
    https = require('https'),
    util = require('util'),
//csrf = require('csurf'),
    helmet = require('helmet'),
    express = require('express'),
    compress = require('compression'),
    bodyParser = require('body-parser'),
    expressValidator = require('express-validator'),
    cookieParser = require('cookie-parser'),
    tooBusy = require('toobusy-js'),
    Authorizator = require('../../common.util.authorizator/authorizator.js').Authorizator,
    RouteUtils = require('../../common.util.route/route.js').RouteUtils,
    routeUtil = new RouteUtils(),
    MainModule = require('./main.js').MainModule,
    app = express(),
    router = express.Router(),
    Notification = require('../../common.notifier'),
    self;

/**
 * Main Module
 * Contains Handlers for Server specific Modules
 *
 * @constructor
 */
function WebModule() {
  WebModule.super_.apply(this, arguments);

  self = this;

  self.utils.logger.log("info", "initializing Web Module...");

  app.enable("jsonp callback");
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(expressValidator());
  app.use(cookieParser());
  app.use(compress());
  app.use(helmet.hsts({
    maxAge: 10886400000,     // Must be at least 18 weeks to be approved by Google
    includeSubdomains: true, // Must be enabled to be approved by Google
    preload: true
  }));
  app.use(helmet.xssFilter());
  app.use(helmet.frameguard());
  app.use(helmet.hidePoweredBy());
  app.use(helmet.ieNoOpen());
  app.use(helmet.noSniff());
  //app.use(helmet.crossdomain());
  //app.use(csrf());

  // middleware which blocks requests when server is too busy
  //TODO add maxlag to configuration
  self.utils.logger.info("busy middleware is set to " + tooBusy.maxLag(5000) + "ms");
  app.use(function (req, res, next) {
    tooBusy() ? res.status(503).json({status: "BUSY"}) : next();
  });

  // middleware for error handling
  app.use(function (err, req, res, next) {
    // mandatory notifier POPUP options
    var options = {
      notification: "",
      request: req
    };
    if (err) {
      var message = new Notification.Message({
        messageType: self.utils.Message.MessageType.ERROR,
        messageTitle: 'Internal Server Error',
        messageText: err.msg || err.message || '500 - Internal Server Error',
        messageData: null
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.DONE;
      options.notification = new Notification(null, message, action, null);
      self.utils.notifier.notify(options, function (err) {
        if (err) {
          self.utils.logger.error('Something went wrong with the notification: ' + JSON.stringify(options.message) + "\n" + err);
        }
      });
    }
    next();
  });

  var server;
  if (self.utils.configuration.get("SERVER.PROTOCOL").indexOf("https") > -1) {
    /* https options */
    var options = {
      key: fs.readFileSync(self.utils.configuration.get("SERVER.CERTIFICATES_PATH") + '/ssl.key'),
      cert: fs.readFileSync(self.utils.configuration.get("SERVER.CERTIFICATES_PATH") + '/ssl.crt')
    };
    server = https.createServer(options, app);
  } else {
    server = http.createServer(app);
  }

  self.utils.app = app;
  self.utils.server = server;
  self.utils.router = router;
  self.utils.authorizator = new Authorizator(self.utils);
  self.utils.route = routeUtil;
  self.utils.express = express;

  startServer(server);

  process.nextTick(function () {
    addDefaultRoutes();
  });
}

function addDefaultRoutes() {

  /**
   * default route for status
   */
  router.get('/status', function (req, res) {
    res.status(200).json({status: "OK"});
  });

  /**
   * Handles all contexts
   * 404 not found (ALWAYS Keep this as the last route)
   */
  router.get('/*', function (req, res) {
    res.redirect("/not-found");
  });

  self.utils.app.use('/', self.utils.router);
}

function startServer(server) {

  /* start server */
  server.listen(self.utils.configuration.get('SERVER.PORT'), self.utils.configuration.get('SERVER.IP'), function () {
    self.utils.logger.log('info', 'node web server [%s] for environment [%s] started on [%s%s:%d | %s%s:%s] ...',
        process.env.NODE_APP, process.env.NODE_ENV,
        self.utils.configuration.get("SERVER.PROTOCOL"),
        self.utils.configuration.get('SERVER.IP'), self.utils.configuration.get('SERVER.PORT'),
        self.utils.configuration.get("SERVER.PROTOCOL"),
        self.utils.configuration.get('SERVER.HOST'), self.utils.configuration.get('SERVER.APACHE_PORT'));
    //process.setgid(config.gid);
    //process.setuid(config.uid);
  });
}

/* inherit Main Module */
util.inherits(WebModule, MainModule);

module.exports.WebModule = WebModule;