/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 08-08-2014
 *
 */

var BaseModule = require('./base.js').BaseModule,
    util = require('util'),
    self;

/**
 * Main Module
 * Contains Handlers for Server specific Modules
 *
 * @constructor
 */
function MainModule() {
    MainModule.super_.apply(this, arguments);

    self = this;

    self.utils.logger.log("info", "initializing General Module...");

    /**
     *  Setup termination handlers
     */
    function terminationHandlers() {

        // Process on exit and signals.
        process.on('exit', function () {
            self.utils.logger.log('info', 'Node server stopped.');
        });

        // Removed 'SIGPIPE', 'SIGSTOP'
        ['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT', 'SIGBUS', 'SIGFPE', 'SIGUSR1',
            'SIGSEGV', 'SIGUSR2', 'SIGTERM'].forEach(function (element) {
                process.on(element, function () {
                    self.utils.logger.log('info', 'Received [%s] terminating server...', element);
                    process.exit();
                });
            });
    }

    /* start termination listeners */
    terminationHandlers();
}

/* inherit Base Module */
util.inherits(MainModule, BaseModule);

module.exports.MainModule = MainModule;