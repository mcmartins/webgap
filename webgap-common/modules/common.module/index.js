/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 07-03-2015
 *
 */

var BaseModule = require('./lib/base.js').BaseModule,
    MainModule = require('./lib/main.js').MainModule,
    WebModule = require('./lib/web.js').WebModule;

module.exports.BaseModule = BaseModule;
module.exports.MainModule = MainModule;
module.exports.WebModule = WebModule;