/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 23-05-2014
 *
 */

var assert = require("assert"),
    Notifier = require('../index').Notifier,
    Message = require('../index').Message,
    notifier;

describe('common.notifier', function () {

  before(function (done) {
    notifier = new Notifier();
    done();
  });

  it('should notify all systems', function (done) {
    var options = {
      system: [notifier.System.POPUP, notifier.System.EMAIL, notifier.System.BACKGROUND],
      message: new Message().generate(Message.MessageType.ERROR, "messages.error.market-application-creation-title",
          "messages.error.market-application-creation", {})

    };
    notifier.notify(options, function (err) {

    });
    done();
  });

});