# README #

This is a notifications manager system.

# Usage #

    var CommonNotifier = require('common.notifier');
    var notifier = new CommonNotifier.Notifier(); // by default loads 3 notification systems
    notifier.register("Teste", Teste); // Teste must inherit AbstractNotificationSystem
    notifier.setDefaultNotificationSystem({notificationSystem: notifier.NotificationSystems.Teste});
    notifier.notify({
        message: new CommonNotifier.Message({
            messageType: CommonNotifier.Message.MessageType.ERROR,
            messageTitle: 'messages.error.title',
            messageText: 'messages.error.text',
            messageData: { errorCode: 111 }
        }),
        anything_you_want: "secret"
        }, function(err) {});

# JS Doc #
