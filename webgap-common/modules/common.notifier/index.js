/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 05-03-2015
 *
 */

var Notification = require('./lib/notification').Notification,
    Logger = require('../common.util.logging').Logger,
    logger = new Logger(),
    ObjectUtils = require('../common.util.object').ObjectUtils,
    utilities = new ObjectUtils();

/**
 * Notifier is a notifications system manager
 * It send messages for specified systems
 *
 * @returns {Notifier}
 * @constructor
 */
function Notifier() {
  var self = this;
  // this eliminates the need to use 'new' keyword when instantiating the module
  if (!(self instanceof Notifier)) {
    return new Notifier();
  }
  logger.info("Initializing Notification system...");
  self.instances = {};
  self.systems = {};
}

/**
 * Sends a message to the specified systems
 * Depending on the notification system, more options might be necessary
 *
 * @param {Object} options
 *        {String} options.defaultNotificationSystem
 *        {Array} options.notificationSystems
 *        {Object} options.message
 * @param callback
 */
Notifier.prototype.notify = function (options, callback) {
  var self = this;
  // handle notification systems
  if (!options.notificationSystems) {
    if (!self.defaultNotificationSystem) {
      logger.error('There are no notification systems registered, cannot continue. Please provide a \'defaultNotificationSystem\' or option \'notificationSystems\'');
      throw new Error('No \'defaultNotificationSystem\' defined and no option \'notificationSystems\' provided.');
    } else {
      options.notificationSystems = [self.defaultNotificationSystem];
    }
  } else {
    if (!Array.isArray(options.notificationSystems)) {
      options.notificationSystems = [options.notificationSystems];
    }
  }
  // ensure the message is not delivered through the same system multiple times
  options.notificationSystems = utilities.Array.removeDuplicates(options.notificationSystems);
  // validate message type
  if (!(options.notification instanceof Notification)) {
    logger.error('Parameter \'option.notification\' is not a valid \'Notification\'.');
    throw new Error('Parameter \'option.notification\' is not a valid \'Notification\'.');
  }
  // notify using the requested systems
  options.notificationSystems.forEach(function (system) {
    var instance = self.instances[system];
    if (instance) {
      instance.notify(options, function (err) {
        // throw err;
      });
    }
  });
  callback();
};

/**
 * Register a new Notification System
 *
 * @param name the name of the client
 * @param Class a class that inherits from AbstractNotificationSystem
 * @throws Error if class doesn't inherits AbstractNotificationSystem
 */
Notifier.prototype.register = function (name, Class) {
  // check if class is an instance of AbstractNotificationSystem
  var instance = new Class();
  if (typeof instance.notify !== 'function') {
    logger.error('The class provided must implement a \'notify\' function. Cannot register Notification System: ' + name);
    throw new Error('The class provided must implement a \'notify\' function. Cannot register Notification System: ' + name);
  }
  logger.info('Notification System registered: ' + name);
  this.instances[name] = instance;
  this.systems[name] = name;
};

/**l
 * Define the default notification system
 *
 * @param options
 *        options.notificationSystem
 */
Notifier.prototype.setDefaultNotificationSystem = function (options) {
  if (options.notificationSystem && this.systems[options.notificationSystem]) {
    this.defaultNotificationSystem = options.notificationSystem;
    logger.info('Default notification system set to: ' + this.defaultNotificationSystem);
  } else {
    logger.error('The Notification System you provided is not registered: ' + options.notificationSystem);
    throw new Error('Invalid Notification System: ' + options.notificationSystem);
  }
};

module.exports.Notifier = Notifier;
module.exports.Notifier.Notification = Notification;