/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 11-04-2015
 *
 */

"use strict";

function Action(name) {
  // always initialize all instance properties
  this.name = name;
  this.status = 'pending';
  this.data = {};
}

module.exports.Action = Action;

module.exports.Action.Status = {
  PENDING: 'pending',
  STARTED: 'started',
  IN_PROGRESS: 'in_progress',
  DONE: 'done',
  FAILED: 'failed'
};