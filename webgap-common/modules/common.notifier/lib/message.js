/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 05-03-2015
 *
 */

"use strict";

/**
 * Message Class
 * Generates a message
 *
 * @param {object} options
 *        {String] options.messageType the message to publish
 *        {String] options.messageTitle the message to publish
 *        {String] options.messageText the message to publish
 *        {String] options.messageData the message to publish
 * @returns {Message}
 * @constructor
 */
function Message(options) {
  var self = this;
  options = options || {};
  // this eliminates the need to use 'new' keyword when instantiating the module
  if (!(self instanceof Message)) {
    return new Message(options);
  }
  // message params
  this.type = options.messageType || 'info';
  this.title = options.messageTitle;
  this.text = options.messageText;
  this.data = options.messageData;
}

module.exports.Message = Message;

/**
 * Message types general
 *
 * @returns {{INFO: string, WARNING: string, SUCCESS: string, ERROR: string}}
 * @constructor
 */
module.exports.Message.MessageType = {
  INFO: "info",
  WARNING: "warning",
  SUCCESS: "success",
  ERROR: "error"
};