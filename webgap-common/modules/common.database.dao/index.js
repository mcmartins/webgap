/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 07-08-2014
 *
 */

var AbstractDatabaseClient = require('./lib/abstract.js').AbstractDatabaseClient,
    Logger = require('../common.util.logging').Logger,
    logger = new Logger(),
    MongoDBDriver = require('./lib/mongodb.js').MongoDBDriver,
    JSONfsDBDriver = require('./lib/jsonfs.js').JSONfsDBDriver;

/**
 * Database Data Access Object
 *
 * @params {databaseDriver}
 * @returns {DatabaseDAO}
 * @constructor
 */
function DatabaseDAO(options) {
  var self = this;
  options = options || {};
  // this eliminates the need to use 'new' keyword when instantiating the module
  if (!(self instanceof DatabaseDAO)) {
    return new DatabaseDAO(options);
  }
  // handle options
  self.databaseDriver = options.databaseDriver || process.env.NODE_APP_DB_DRIVER;
  logger.info('Initializing database connection using driver: ' + self.databaseDriver);
  // register default clients
  self.clients = {};
  // TODO remove default registration, this should be initialized based on options.databaseDriver or process.env.NODE_APP_DB_DRIVER
  self.register("jsonfs", JSONfsDBDriver, options);
  //this.register("mongodb", MongoDBDriver, options);
}

/**
 * Initialize Data Access Object using the default system
 *
 * @param callback
 * @returns {*}
 */
DatabaseDAO.prototype.getClient = function (callback) {
  // resolve client for identity provider
  var instance = this.clients[this.databaseDriver];
  return callback(null, instance);
};

/**
 * Register a new Database System
 *
 * @param name the name of the client
 * @param Class a class that inherits from AbstractDatabaseClient
 * @param options the options for the database system instance
 * @throws Error if class doesn't inherits AbstractDatabaseClient
 */
DatabaseDAO.prototype.register = function (name, Class, options) {
  // check if class is an instance of AbstractDatabaseClient
  var instance = new Class(options);
  if (!(instance instanceof AbstractDatabaseClient)) {
    logger.error('Client is not a valid AbstractDatabaseClient.');
    throw new Error('Client is not a valid AbstractDatabaseClient.');
  }
  this.clients[name] = instance;
};

module.exports.DatabaseDAO = DatabaseDAO;
