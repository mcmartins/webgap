/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 07-08-2014
 *
 */
var Logger = require('../../common.util.logging').Logger,
    logger = new Logger();
/**
 * Abstract Database Client
 *
 * @param {Object} options
 *        {String} options.collectionName
 * @returns {AbstractDatabaseClient}
 * @constructor
 */
function AbstractDatabaseClient(options) {
  var self = this;
  // this eliminates the need to use 'new' keyword when instantiating the module
  if (!(self instanceof AbstractDatabaseClient)) {
    return new AbstractDatabaseClient(options);
  }
  // database name shall be present
  if (!options.collectionName) {
    throw new Error('Database Name should be defined!');
  }
  // initialize the database name
  self.collectionName = options.collectionName;
  self.logger = logger;
}

/**
 * Finds all objects by criteria
 * If no criteria returns everything
 *
 * @param criteria the criteria
 * @param callback
 */
AbstractDatabaseClient.prototype.find = function (criteria, callback) {
  return callback(new Error('Not implemented'));
};

/**
 * Finds an object by criteria
 * If no criteria throws an Error
 *
 * @param criteria the criteria (mongo query)
 * @param callback
 */
AbstractDatabaseClient.prototype.findOne = function (criteria, callback) {
  return callback(new Error('Not implemented'));
};

/**
 * Finds a objects by criteria and updates it
 *
 * @param criteria the criteria
 * @param callback
 */
AbstractDatabaseClient.prototype.findAndModify = function (criteria, updateCriteria, callback) {
  return callback(new Error('Not implemented'));
};

/**
 * Persists  an object
 *
 * @param object the object to persist
 * @param callback
 */
AbstractDatabaseClient.prototype.insert = function (object, callback) {
  return callback(new Error('Not implemented'));
};

/**
 * Updates an existent object
 *
 * @param criteria the criteria to update
 * @param object the object to update
 * @param callback
 */
AbstractDatabaseClient.prototype.update = function (criteria, object, callback) {
  return callback(new Error('Not implemented'));
};

/**
 * Removes an object
 *
 * @param criteria the id to remove
 * @param callback
 */
AbstractDatabaseClient.prototype.remove = function (criteria, callback) {
  return callback(new Error('Not implemented'));
};

module.exports.AbstractDatabaseClient = AbstractDatabaseClient;
