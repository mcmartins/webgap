/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 07-08-2014
 *
 */

var AbstractDatabaseClient = require('./abstract.js').AbstractDatabaseClient,
    MongoDBClient = require('mongodb').MongoClient,
    util = require('util');

/**
 * MongoDB driver
 *
 * @param {Object} options
 *        {String} options.connectionString
 *        {String} options.collectionName
 * @returns {MongoDBDriver}
 * @constructor
 */
function MongoDBDriver(options) {
  MongoDBDriver.super_.apply(this, arguments);

  var self = this;
  options = options || {};
  // this eliminates the need to use 'new' keyword when instantiating the module
  if (!(self instanceof MongoDBDriver)) {
    return new MongoDBDriver(options);
  }
  // set global options
  self.connectionString = options.connectionString;
  MongoDBClient.connect(self.connectionString, function (err, database) {
    if (err) {
      self.logger.error('Couldn\'t connect to database using the connection string: ' + self.connectionString);
      throw err;
    }
    self.logger.log('successfully connected to MongoDB...');
    self.logger.info('Created connection to collection: ' + self.collectionName);
    self.database = database;
  });
}

/* inherit Abstract Module */
util.inherits(MongoDBDriver, AbstractDatabaseClient);

/**
 * {@inheritDoc}
 */
MongoDBDriver.prototype.find = function (criteria, callback) {
  var collection = this.database.collection(this.collectionName);
  collection.find(criteria).toArray(callback);
};

/**
 * {@inheritDoc}
 */
MongoDBDriver.prototype.findOne = function (criteria, callback) {
  var collection = this.database.collection(this.collectionName);
  collection.findOne(criteria, callback);
};

/**
 * {@inheritDoc}
 */
MongoDBDriver.prototype.findAndModify = function (criteria, updateCriteria, callback) {
  var collection = this.database.collection(this.collectionName);
  collection.findAndModify({
    query: criteria,
    update: updateCriteria,
    upsert: false
  }, callback);
};

/**
 * {@inheritDoc}
 */
MongoDBDriver.prototype.insert = function (object, callback) {
  var collection = this.database.collection(this.collectionName);
  collection.insert(object, callback);
};

/**
 * {@inheritDoc}
 */
MongoDBDriver.prototype.update = function (criteria, object, callback) {
  var collection = this.database.collection(this.collectionName);
  collection.update(criteria, object, callback);
};

/**
 * {@inheritDoc}
 */
MongoDBDriver.prototype.remove = function (criteria, callback) {
  var collection = this.database.collection(this.collectionName);
  collection.remove(criteria, callback);
};

module.exports.MongoDBDriver = MongoDBDriver;