/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 07-08-2014
 *
 */

require('babel/register');

var AbstractDatabaseClient = require('./abstract.js').AbstractDatabaseClient,
    jsonfs = require('json_file_system'),
    jsonQuery = require('json-criteria').test,
    util = require('util');

/**
 * JSON File System database driver
 *
 * @param {Object} options
 *        {String} options.connectionString
 *        {String} options.collectionName
 * @returns {JSONfsDBDriver}
 * @constructor
 */
function JSONfsDBDriver(options) {
  JSONfsDBDriver.super_.apply(this, arguments);

  var self = this;
  // this eliminates the need to use 'new' keyword when instantiating the module
  if (!(self instanceof JSONfsDBDriver)) {
    return new JSONfsDBDriver(options);
  }
  // the default path where the database should be stored
  self.connectionString = options.connectionString || '/tmp';
  // initialize jsonfs connection to database
  self.database = jsonfs.connect(self.connectionString, [self.collectionName]);
  self.logger.info('Created connection to collection: ' + self.collectionName);
}

/* inherit Abstract Module */
util.inherits(JSONfsDBDriver, AbstractDatabaseClient);

/**
 * {@inheritDoc}
 */
JSONfsDBDriver.prototype.find = function (criteria, callback) {
  var collection = this.database[this.collectionName].find();
  var object = [];
  if (criteria) {
    for (var i = collection.length - 1; i >= 0; i--) {
      var result = jsonQuery(collection[i], criteria);
      if (result) {
        object.push(collection[i]);
      }
    }
  } else {
    object = collection;
  }
  return callback(null, object);
};

/**
 * {@inheritDoc}
 */
JSONfsDBDriver.prototype.findOne = function (criteria, callback) {
  if (!criteria) {
    return callback(new Error('No criteria specified!'));
  }
  this.find(criteria, function (err, collection) {
    if (err) {
      callback(err);
    }
    return callback(null, collection.length >= 0 ? collection[0] : undefined);
  });
};

/**
 * {@inheritDoc}
 */
JSONfsDBDriver.prototype.findAndModify = function (criteria, updateCriteria, callback) {
  var self = this;
  if (!criteria) {
    return callback(new Error('No criteria specified!'));
  }
  this.findOne(criteria, function (err, collection) {
    if (err) {
      callback(err);
    }
    if (collection) {
      for (var propertyName in updateCriteria) {
        if (updateCriteria.hasOwnProperty(propertyName) && collection.hasOwnProperty(propertyName)) {
          collection[propertyName] = updateCriteria[propertyName];
        }
      }
      self.update({id: collection.id}, collection, callback)
    }
  });
};

/**
 * {@inheritDoc}
 */
JSONfsDBDriver.prototype.insert = function (object, callback) {
  if (!object) {
    return callback(Error('No object passed to persist!'));
  }
  this.database[this.collectionName].save(object);
  return callback(null);
};

/**
 * {@inheritDoc}
 */
JSONfsDBDriver.prototype.update = function (criteria, object, callback) {
  if (!criteria) {
    return callback(Error('No criteria defined!'));
  }
  if (!object) {
    return callback(Error('No object passed to persist!'));
  }
  var options = {
    multi: false,
    upsert: false
  };
  this.database[this.collectionName].update(criteria, object, options);
  return callback(null);
};

/**
 * {@inheritDoc}
 */
JSONfsDBDriver.prototype.remove = function (criteria, callback) {
  if (!criteria) {
    return callback(Error('No criteria defined!'));
  }
  this.database[this.collectionName].remove(criteria);
  return callback(null);
};

module.exports.JSONfsDBDriver = JSONfsDBDriver;
