/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

var moment = require('moment'),
    uuid = require('node-uuid'),
    token = require('../common.token/index.js'),
    configuration = require('../common.configuration').load();

/**
 * Handles the creation of messages based in JSON Schemas
 * @type {{Client: {generate: generate, Type: {INFO: string, WARNING: string, SUCCESS: string, ERROR: string}}, Server: {Log: {generate: generate, Type: {INFO: string, DEBUG: string, ERROR: string}}, Request: {generate: generate}, Response: {generate: generate, Status: {OK: string, INVALID_REQUEST: string, REQUEST_DENIED: string, ERROR: string}}, Token: {generate: generate}}}}
 */
module.exports = {

    /**
     * Client messages
     */
    Client: {

        /**
         * Generates an user message based on the JSON schema
         * @param type
         * @param titleKey
         * @param textKey
         * @param data
         * @returns {{type: *, title: *, text: *}}
         */
        generate: function (type, titleKey, textKey, data) {
            /* the message is internationalized using ejs */
            return {
                type: type,
                title: titleKey,
                text: textKey
            };
        }
    },

    /**
     * Server messages
     */
    Server: {

        /**
         * Log messages
         */
        Log: function (level, message) {
            return {
                level: level,
                message: message
            };
        },

        /**
         * Requests
         */
        Request: function () {
            var id = uuid.v4();
            return {
                id: id,
                token: index.HS512.encode(this.Token()),
                replyToChannel: '/services/response/' + id.replace(/-/g, ""),
                data: ""
            };
        },

        /**
         * Responses
         */
        Response: function () {
            return {
                id: uuid.v4(),
                token: index.HS512.encode(this.Token()),
                status: "",
                error: {
                    code: "",
                    details: ""
                },
                result: {
                    data: "",
                    url: ""
                }
            };
        },

        /**
         * Tokens
         */
        Token: function (user) {
            return {
                host: configuration.get('SERVER.HOST'),
                creator: user || 'System',
                issuedAt: moment(),
                expiresAt: moment().add(configuration.get('GENERAL.TOKEN.VALIDITY_TIME'), 'milliseconds')
            };
        },

        /**
         * Response status
         */
        Status: {
            OK: "OK",
            INVALID_REQUEST: "INVALID_REQUEST",
            REQUEST_DENIED: "REQUEST_DENIED",
            ERROR: "ERROR"
        }
    },

    /**
     * Message types general
     */
    Type: {
        INFO: "info",
        WARNING: "warning",
        SUCCESS: "success",
        ERROR: "error"
    },

    /**
     * Messages types for log
     */
    LogType: {
        INFO: "info",
        DEBUG: "debug",
        ERROR: "error"
    }

};
