/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 30-07-2014
 *
 */

/**
 * Common utility methods
 *
 * @returns {ObjectUtils}
 * @constructor
 */
function ObjectUtils() {
    var self = this;
    // this eliminates the need to use 'new' keyword when instantiating the module
    if (!(self instanceof ObjectUtils)) {
        return new ObjectUtils();
    }
}

ObjectUtils.prototype.Object = {};
ObjectUtils.prototype.Array = {};

/**
 * Clones Javascript Objects
 *
 * @param obj the object to clone
 * @returns {*} the cloned object
 */
ObjectUtils.prototype.Object.clone = function (obj) {
    switch (this.getType(obj)) {
        case "[object Undefined]":
            return;
        case "[object Array]":
            return obj.slice(0);
        default :
            return JSON.parse(JSON.stringify(obj));
    }
};

/**
 * Returns the type of an object
 *
 * @param object
 * @returns {string}
 */
ObjectUtils.prototype.Object.getType = function (object) {
    return Object.prototype.toString.call(object);
};

/**
 * Returns the index of an object by checking for a specified attribute value. Returns -1 otherwise.
 *
 * @param array the array to lookup
 * @param attr the attribute name
 * @param value the value to check
 * @returns {number} the index
 */
ObjectUtils.prototype.Array.indexOf = function (array, attr, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i].hasOwnProperty(attr) && array[i][attr] === value) {
            return i;
        }
    }
    return -1;
};

/**
 * Returns the array without duplicates
 *
 * @param arr the array to remove duplicates
 * @returns {Array} the array with unique elements
 */
ObjectUtils.prototype.Array.removeDuplicates = function (arr) {
    var i,
        len = arr.length,
        out = [],
        obj = {};

    for (i = 0; i < len; i++) {
        obj[arr[i]] = 0;
    }
    for (i in obj) {
        out.push(i);
    }
    return out;
};

module.exports.ObjectUtils = ObjectUtils;