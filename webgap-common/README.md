# README #

# Common #

Contains shared utility modules:

* Configuration
* Cache
* Token
* Messages
* Json (Schemas, Validation)
* Logger