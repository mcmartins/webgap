# README #

# Node #

The **Node** is responsible for serving GAP through a web-based shell. It can handle requests for other tools processing / access.
This application should be replicated, in other VMs, to serve more GAP consoles / other tools.

# Architecture #

## Overview ##

![Node.png](https://bytebucket.org/mcmartins/webgap/raw/2facd633ae8edd936fe0dbc864af82b2d1a7c52b/webgap-etc/images/Node.png)

## Low Level ##

![Node-GAPLowLevel.png](https://bytebucket.org/mcmartins/webgap/raw/2facd633ae8edd936fe0dbc864af82b2d1a7c52b/webgap-etc/images/Node-GAPLowLevel.png)