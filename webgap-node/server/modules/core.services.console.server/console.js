/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

var pty = require('../../../../webgap-common/node_modules/pty.js'),
    io = require('../../../../webgap-common/node_modules/socket.io'),
    terminal = require('../../../../webgap-common/node_modules/term.js');

module.exports = function (utils) {

    utils.app.use(function (req, res, next) {
        var setHeader = res.setHeader;
        res.setHeader = function (name) {
            switch (name) {
                case 'Cache-Control':
                case 'Last-Modified':
                case 'ETag':
                    return;
            }
            return setHeader.apply(res, arguments);
        };
        next();
    });

    utils.app.use(terminal.middleware());

    io = io.listen(utils.server);

    io.sockets.on('connection', function (socket) {
        var buff = [];

        socket.on('init', function (app) {

            var term = pty.fork('/home/mcmartins/' + app, [], {
                name: app,
                cols: 104,
                rows: 41,
                useStyle: true,
                cursorBlink: true,
                scrollback: 10000
            });

            term.on('data', function (data) {
                return !socket
                    ? buff.push(data)
                    : socket.emit('data', data);
            });

            socket.on('data', function (data) {
                term.write(data);
            });

            socket.on('disconnect', function () {
                term.write('');
                term.destroy();
            });

            socket.on('error', function () {
                term.write('');
                term.destroy();
            });

            socket.on('close', function () {
                term.write('');
                term.destroy();
            });

            while (buff.length) {
                socket.emit('data', buff.shift());
            }
        });
    });
};
