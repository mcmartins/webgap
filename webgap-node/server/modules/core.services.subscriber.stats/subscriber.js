/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

var systemMemory = require('../../../../webgap-common/modules/common.system.memory'),
    systemCPU = require('../../../../webgap-common/modules/common.system.cpu'),
    systemPlatform = require('../../../../webgap-common/modules/common.system.platform');

module.exports = function (utils) {
    utils.client.on("connect", function () {
        utils.client.unsubscribe(utils.configuration.get('AMQP.CHANNELS.STATS'));
        utils.client.subscribe(utils.configuration.get('AMQP.CHANNELS.STATS'))
        utils.client.on("message", function (topic, message) {
            if (topic != utils.configuration.get('AMQP.CHANNELS.STATS')) {
                return;
            }
            message = JSON.parse(message);
            var memoryPercentage = systemMemory.getFreeMemoryPercentage() * 100;
            var response = utils.message.Server.Response();
            //var gapCount = execSync("ps aux | grep -v grep | grep gap | wc -l");
            response.result.data = {
                NODE: {
                    NAME: utils.client._clientId,
                    HOST: utils.configuration.get('SERVER.HOST')
                },
                MEMORY: {
                    TOTAL: utils.formatter.formatBytes(systemMemory.getTotalMemory()),
                    FREE: utils.formatter.formatBytes(systemMemory.getFreeMemory()),
                    FREE_PERCENTAGE: utils.formatter.formatPercentage(memoryPercentage)
                },
                CPU: {
                    LOAD_AVERAGE: utils.formatter.formatNumber(systemCPU.getLoadAverage(1)) +
                        " | " + utils.formatter.formatNumber(systemCPU.getLoadAverage(5)) +
                        " | " + utils.formatter.formatNumber(systemCPU.getLoadAverage(15)),
                    CPU_COUNT: systemCPU.getNumberOfCPUs()
                },
                PLATFORM: {
                    UP_TIME: utils.formatter.formatSeconds(systemPlatform.getUpTime()),
                    PROCESS_UP_TIME: utils.formatter.formatSeconds(systemPlatform.getProcessUpTime()),
                    PLATFORM: systemPlatform.getPlatform(),
                    //todo implement
                    GAP_COUNT: "N/A"
                },
                STATUS: memoryPercentage < 50 && memoryPercentage > 20 ? "warning" : memoryPercentage < 20 ? "danger" : "info"
            };
            utils.client.publish(message.replyToChannel, JSON.stringify(response));
            utils.logger.log("info", "Request on channel [%s] processed...", utils.configuration.get('AMQP.CHANNELS.STATS'));
        });
    });
};