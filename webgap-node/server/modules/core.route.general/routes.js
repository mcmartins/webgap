/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

var pty = require('../../../../webgap-common/node_modules/pty.js');

/**
 * Handles application routes
 */
module.exports = function (utils) {

    /**
     * Handles console requests
     * Checks for valid requests
     */
    utils.router.get('/service/:app', function (req, res) {
        var url = utils.configuration.get('SERVER.HOST') + ':' + utils.configuration.get('SERVER.PORT');
        res.render('index.html', { url: url, app: req.sanitize('app').toString() });
    });

};
