/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

var systemMemory = require('../../../../webgap-common/modules/common.system.memory');

module.exports = function (utils) {
    utils.client.on("connect", function () {
        utils.client.unsubscribe(utils.configuration.get('AMQP.CHANNELS.REQUEST'));
        utils.client.subscribe(utils.configuration.get('AMQP.CHANNELS.REQUEST'));
        utils.client.on("message", function (topic, message) {
            if (topic != utils.configuration.get('AMQP.CHANNELS.REQUEST')) {return;}
            // decode expected message
            message = JSON.parse(message);
            var data = utils.tokenizer.HS512.decode(message.data);
            var neededMemory = JSON.parse(data).ram;
            var freeMemory = systemMemory.getFreeMemory();
            //todo should be a different module handling system health
            if (freeMemory < neededMemory + 100) {
                utils.logger.log("info", "I can provide a GAP console...Memory Available [%s]", freeMemory);
                var url = utils.message.Server.Response();
                url.result.data = utils.tokenizer.HS512.encode(neededMemory);
                var response = utils.message.Server.Response();
                response.token = message.token;
                response.status = utils.message.Server.Status.OK;
                response.result.url = [utils.configuration.get('SERVER.PROTOCOL'), utils.configuration.get('SERVER.HOST'), ':', utils.configuration.get('SERVER.PORT'), '/gap/', utils.tokenizer.HS512.encode(neededMemory)].join('');
                utils.client.publish(message.replyToChannel, JSON.stringify(response));
            } else {
                utils.logger.log("info", "Cannot instantiate GAP here, resources are too low sorry...Memory Available [%s]", freeMemory);
            }
            utils.logger.log("info", "Request on channel [%s] processed...", utils.configuration.get('AMQP.CHANNELS.GAP'));
        });
    });
};