/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 01-06-2014
 *
 */

var ejs = require('../../webgap-common/node_modules/ejs'),
    WebModule = require('../../webgap-common/modules/common.module').WebModule,
    //requireUtil = require('../../webgap-common/modules/common.util.require'),
    util = require('util');

/**
 * Main Module
 * Contains Handlers for Server specific Modules
 *
 * @constructor
 */
function WebGAPNode() {
    WebGAPNode.super_.apply(this, arguments);

    var self = this;

    self.utils.app.engine('.html', ejs.__express);
    self.utils.app.set('view engine', 'html');
    self.utils.app.set('views', __dirname + '/../client/views');
    self.utils.app.use(self.utils.express.static(__dirname + '/../public'));

    /* initialize routes */
    require("./modules/core.route.general")(self.utils);

    /* initialize console */
    require("./modules/core.services.console.server")(self.utils);

    /* initialize publisher subscriber */
    //require("./modules/core.pubsub.client")(self.utils);

    /* other subscribers are loaded automatically */
    //requireUtil.modules(__dirname + "/modules", "core.services.subscriber.*", self.utils);

}

/* Inherit Web Module */
util.inherits(WebGAPNode, WebModule);

module.exports = WebGAPNode;
