
Conference Feedback

sage math cloud:
    what offers?
	is it free?
	how it works?
	
webgap:
    what can it offer? customized for teaching/learning/training
	is it free? yes
	how it works?
	
	reply to manuel delgado

webgap future:
    mature state presentation in st andrews
	identify the problems detected at the conference
	check browser compatibility
	finish ide
	add possibility to restart a node
	add information on availability of the system
	add more information for the user
	add sharing features
	add integration with bitbucket, github, etc
	add integration with dropbox, box, onedrive and googledrive
	add possibility to save/load workspaces
	add start script
	add prover9 as a proof of concept
	how to install the whole system?
	how to update gap? centralized?
	possibility to use a specific version of GAP
	how to load packages? packages repository? what about custom/testing/in development packages?