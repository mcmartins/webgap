#!/bin/bash
#
#
SCRIPT="$0"
ACTION="$1"
APP="$2"
ALIAS="$3"

source /usr/bin/rhcsh

function error() {
	if [ $1 != 0 ]; then
		echo -e "\n\n\t Something went wrong :("
		exit 100
	fi
}

function configHandler() {
    if [[ `grep "NODE_APP" $PATH_REMOTE_NODE_CONF | wc -l` == 0 ]]; then
        cat $PATH_CHECKOUT/$APP/conf/node.env >> $PATH_REMOTE_NODE_CONF
        error $?
    fi
    sed "s/%INSTANCE%/$ALIAS/g" $PATH_REMOTE_COMMON/server/modules/common.config/index.js > $PATH_REMOTE_COMMON/server/modules/common.config/temp
    error $?
    mv $PATH_REMOTE_COMMON/server/modules/common.config/temp $PATH_REMOTE_COMMON/server/modules/common.config/index.js
    error $?
}

function commonHandler() {
    mkdir -p $PATH_REMOTE_COMMON
    mv $PATH_CHECKOUT/Common/* $PATH_REMOTE_COMMON
    error $?
    mkdir -p $PATH_REMOTE_COMMON/logs
}

function appHandler() {
    mv $PATH_CHECKOUT/$APP/* ~/app-root/runtime/repo
    error $?
}

function gapHandler() {
    mv ~/app-root/runtime/gap-latest/* ~/app-root/runtime/dependencies/
    error $?
    mv ~/app-root/runtime/dependencies/gap* ~/app-root/runtime/dependencies/gap
    error $?
}

if [[ -z "$ACTION" || -z "$APP" || ! "$APP" =~ Node|Portal|Orchestrator || ! "$ACTION" =~ update|clean-update ]]; then
	echo "
	To deploy (install or update already existing deployment) run:

	./deploy.sh < action > [ environment ] (alias)

	Where:
	 'action'     is one of: update, clean-update
	 'enviroment' is one of: Portal, Orchestrator, Node, common
	 'alias'      is the alias gap instance number for the gear if it is a node (e.g. 01, 02, 02, ... xy)
	"
	exit 100
fi

echo "Starting...0%"
if [[ "$ACTION" == "clean-update" ]]; then
    echo "Cleaning directories...5%"
    find ~/app-root/runtime/repo -mindepth 1 -maxdepth 1 -not -name "*.db" -not -name ".*" -exec rm -rf {} \;
    rm -rf ~/app-root/runtime/Common
    rm -rf ~/app-root/runtime/webgap
    rm -rf ~/app-root/runtime/gap*
    rm -rf ~/app-root/runtime/dependencies/gap*
fi

echo "Checking out code... 10%"
git clone https://mcmartins@bitbucket.org/mcmartins/webgap.git

echo "Updating Common module...30%"
commonHandler

if [[ "$APP" != "Common" ]]; then
    if [[ "$APP" == "Node" ]]; then
        echo "Checking out GAP... 40%"
        git clone https://mcmartins@bitbucket.org/mcmartins/gap-latest.git
        echo "Updating GAP...50%"
        gapHandler
    else
        echo "Only a Node needs GAP to be updated...skipping...50%"
    fi
    echo "Updating configurations...60%"
    configHandler
    echo "Updating $APP module...80%"
    appHandler
fi

echo "Cleaning temporary files...95%"
rm -rf ~/app-root/runtime/webgap
rm -rf ~/app-root/runtime/gap*
echo "Restarting server...99%"
echo 1 | ctl_app restart &
echo "Finished...100%"