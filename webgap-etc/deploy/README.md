# Production Deployment Tasks

1 - create deploy structure directory;

2 - handle resources:

	-minify js
	-minify css
	-compress img
	-change html *.css *.js ref to *min.css *min.js

3 - generate production config;

4 - deploy remotely;