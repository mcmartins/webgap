# README #

This project is organized in 2 main applications and they share a Common set of modules. All the communication is made using **JSON** as interface messaging and is secured using **Timed Tokens** encoded in [RS256 / HS512](http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html).

# Portal #

The **Portal** is the main application. It can be accessed through *http* and it allows to use the entire system:

* Authentication / Authorization (using external providers)
* Administration (users and subscription management, statistics on server resources, etc)*
* IDE - integrated development environment - To ease the usage GAP programming*
* Cloud storage access*
* Console requests / access
* Other tools requests / access*

**NOTE:** * indicates features not implemented yet

# Node #

The **Node** is responsible for serving GAP through a web-based shell. It can handle requests for other tools processing / access.
This application should be replicated, in other VMs, to serve more GAP consoles / other tools.

# Common #

Contains shared utility modules:

* Configuration
* Cache
* Token
* Messages
* Json (Schemas, Validation)
* Logger

# Diagrams #

## Overview ##

![VjbAEOgQfVIUey1x-2D2DD.png](https://cacoo.com/diagrams/VjbAEOgQfVIUey1x-2D2DD.png)

## Communications ##

![VjbAEOgQfVIUey1x-BCB5C.png](https://cacoo.com/diagrams/VjbAEOgQfVIUey1x-BCB5C.png)
