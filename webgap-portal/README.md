# README #

# Portal #

The **Portal** is the main application. It can be accessed through *http* and it allows to use the entire system:

* Authentication / Authorization (using external providers)
* Administration (users and subscription management, statistics on server resources, etc)*
* IDE - integrated development environment - To ease the usage GAP programming*
* Cloud storage access*
* Console requests / access
* Other tools requests / access*

**NOTE:** * indicates features not implemented yet

# Architecture #

## Overview ##

![Portal.png](https://bytebucket.org/mcmartins/webgap/raw/2facd633ae8edd936fe0dbc864af82b2d1a7c52b/webgap-etc/images/Portal.png)
