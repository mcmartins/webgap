#!/bin/env node
/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 11-05-2014
 *
 */

// FIXME :S use one of https://gist.github.com/branneman/8048520 | https://github.com/substack/browserify-handbook#avoiding-
// TODO remove it and add all modules to npm
/* BAD BAD BAD hack require in order to avoid paths like '../../../' */
global.requireCommon = function (name) {
    var module = undefined;
    try {
        // check common modules
        module = require(__dirname + '/../webgap-common/modules/' + name);
    } catch (error) {
        // check common node_modules
        //console.log(error);
        module = require(__dirname + '/../webgap-common/node_modules/' + name);
    }
    return module;
};

requireCommon('newrelic');

/* instantiate application */
var WebGAPPortal = require('./server/app');
new WebGAPPortal();