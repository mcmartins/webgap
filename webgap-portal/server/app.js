/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 03-05-2014
 *
 */

var util = require('util'),
    NotifierHelper = require('./modules/core.util.notifier.helper').NotifierHelper,
    WebModule = requireCommon('common.module').WebModule,
    favicon = requireCommon('serve-favicon'),
    session = requireCommon('express-session'),
    ejs = requireCommon('ejs'),
    i18n = requireCommon('i18n');

/**
 * WebGAP Portal initialize scripts
 */
function WebGAPPortal() {
  WebGAPPortal.super_.apply(this, arguments);

  var self = this;

  self.utils.logger.log("info", "initializing Portal...");

  // express general configuration
  self.utils.app.engine('.html', ejs.__express);
  self.utils.app.set('view engine', 'html');
  self.utils.app.set('views', __dirname + '/../client');
  self.utils.app.use(self.utils.express.static(__dirname + '/../public'));
  self.utils.app.use(self.utils.express.static(__dirname + '/../schemas'));
  self.utils.app.use(favicon(__dirname + '/../public/img/favicon.ico'));
  self.utils.app.use(i18n.init);

  // passport session is initialized inside the module 'core.route.auth' called by the router
  self.utils.app.use(session({
        secret: self.utils.configuration.get('GENERAL.COOKIE.SECRET'),
        maxAge: new Date(Date.now() + self.utils.configuration.get('GENERAL.COOKIE.MAX_AGE')),
        cookie: {
          httpOnly: true,
          secure: true
        },
        key: "WebGAP.sid",
        saveUninitialized: true,
        resave: true
      })
  );

  self.utils.ejs = ejs;
  self.utils.i18n = i18n;
  self.utils.notifierHelper = new NotifierHelper(self.utils);

  // TODO add validation for params router.param('id', /^\d+$/); uuid
  /* initialize template filter */
  require('./modules/core.util.template.filter')(self.utils);

  /* initialize internationalization */
  require('./modules/core.util.i18n')(self.utils);

  /* initialize publisher subscriber */
  requireCommon('common.amqp.client')(self.utils);

  /* initialize router */
  require('./modules/core.route')(self.utils);

  /* initialize the notifications system */
  require('./modules/core.notification.system')(self.utils);

}

/* inherit Web Module */
util.inherits(WebGAPPortal, WebModule);

module.exports = WebGAPPortal;