/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 03-05-2014
 *
 */

var passport = requireCommon('passport'),
    AutomaticRequireUtils = requireCommon('common.util.require').AutomaticRequireUtils,
    automaticRequireUtils = new AutomaticRequireUtils(),
    userService = require('../core.data.service.user'),
    template = require('../core.util.template');

module.exports = {

    init: function (utils) {

        utils.app.use(passport.initialize());
        utils.app.use(passport.session());

        passport.serializeUser(function (profile, done) {
            // the following method handles logged in, existing users not logged in and new users
            userService.createOrUpdateUser(profile, function (err, userId) {
                if (err) {
                    // something is wrong
                    done(err);
                } else {
                    // good to go
                    done(null, userId);
                }
            });
        });

        passport.deserializeUser(function (userId, done) {
            // TODO create session user
            userService.getUserById(userId, function(err, user) {
                if (err) {
                    // something is wrong
                    done(err);
                } else {
                    // good to go
                    done(null, user);
                }
            });
        });

        utils.passport = passport;

        /* other routes for authentication methods are loaded automatically */
        automaticRequireUtils.require(__dirname + "/../", "core.route.auth.*", utils);

        /* terms and conditions acceptance before login */
        utils.router.get('/auth/login', function (req, res) {
            var requestVariables = template.generate(req);
            requestVariables.providers = template.convertCloudProviders(req);
            res.render('landing/login.html', requestVariables);
        });

        /* revoke access to account */
        utils.router.get('/auth/revoke/:provider', utils.authorizator.isAuthorized(['user']), function (req, res) {
            var provider = req.params.provider;
            // mandatory notifier POPUP options
            var options = {
                message: "",
                request: req
            };
            if (provider) {
                userService.revokeProvider(req.user, provider, function (err) {
                    if (err) {
                        options.message = utils.notifierHelper.ERROR.AUTH_REVOKE_ACCESS_GENERIC(provider);
                        utils.notifier.notify(options, utils.notifierHelper.handleError);
                    } else {
                        options.message = utils.notifierHelper.SUCCESS.AUTH_REVOKE_ACCESS(provider);
                        utils.notifier.notify(options, utils.notifierHelper.handleError);
                    }
                    res.redirect('/');
                });
            } else {
                options.message = utils.notifierHelper.ERROR.AUTH_REVOKE_ACCESS_MISSING_PARAM();
                utils.notifier.notify(options, utils.notifierHelper.handleError);
                res.redirect('/');
            }
        });

        /* logout */
        utils.router.get('/auth/logout', utils.authorizator.isAuthorized(['user']), function (req, res) {
            req.logout();
            res.redirect('/');
        });
    }
};