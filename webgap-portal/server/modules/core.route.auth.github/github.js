/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 23-05-2014
 *
 */

var GitHubStrategy = requireCommon('passport-github').Strategy;

module.exports = function (utils) {

    var strategy = new GitHubStrategy({
            clientID: utils.configuration.get('MODULE.AUTHENTICATION.GITHUB.CLIENT_ID'),
            clientSecret: utils.configuration.get('MODULE.AUTHENTICATION.GITHUB.CLIENT_SECRET'),
            callbackURL: [utils.configuration.get('SERVER.PROTOCOL'), utils.configuration.get('SERVER.HOST'), ':',
                utils.configuration.get('SERVER.APACHE_PORT'), '/auth/github/oauth2callback'].join(''),
            passReqToCallback: true
        }, function (req, accessToken, refreshToken, profile, done) {
            process.nextTick(function () {
                profile.accessToken = accessToken || null;
                profile.tokenType = null;
                profile.expiresIn = null;
                profile.refreshToken = refreshToken || null;
                profile.loggedUser = req.user || null;
                return done(null, profile);
            });
        }
    );

    utils.passport.use(strategy);

    utils.router.get('/auth/github', utils.passport.authenticate('github'));

    utils.router.get('/auth/github/oauth2callback', utils.passport.authenticate('github', {failureRedirect: '/auth/login'}),
        function (req, res) {
            res.redirect('/account/profile');
        });

};