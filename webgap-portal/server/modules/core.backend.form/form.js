/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 12-10-2014
 *
 */

var needle = requireCommon('needle');

/**
 * Alpaca Generic Form helper
 *
 * @type {{build: Function, build: Function}}
 */
module.exports = {

    /**
     * Returns a json object with the following structure:
     * <pre>
     *     {
     *      schema: value,
     *      data: value,
     *      submitUrl: value,
     *      cancelUrl: value
     *     }
     * </pre>
     *
     * @param schemaURL the url of the schema to render
     * @param data the data to load
     * @param submitUrl the form submission url
     * @param cancelUrl the cancel redirect url
     * @param callback the callback receives the object form
     */
    buildFromURL: function (schemaURL, data, submitUrl, cancelUrl, callback) {
        needle.get(schemaURL, {
            compressed: true,
            rejectUnauthorized: false
        }, function (err, res) {
            var schema = res ? res.body : undefined;
            var form = {
                schema: JSON.stringify(schema),
                data: data ? JSON.stringify(data) : '',
                submitUrl: submitUrl || '#',
                cancelUrl: cancelUrl || '/'
            };
            callback(form)
        });
    },

    /**
     * Returns a json object with the following structure:
     * <pre>
     *     {
     *      schema: value,
     *      data: value,
     *      options: value,
     *      view: value,
     *      action: value,
     *     }
     * </pre>
     *
     * @param resource the url of the schema to render
     * @param data the data to load
     * @param action the action
     */
    buildLocal: function (resource, data, action) {
        return {
            schema: '/alpaca/schemas/'+resource,
            data: data ? JSON.stringify(data) : JSON.stringify({}),
            options: '/alpaca/options/'+resource,
            view: '/alpaca/views/'+resource,
            action: action || '/'
        };
    }
};