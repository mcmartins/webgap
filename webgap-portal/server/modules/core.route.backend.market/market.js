/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 03-05-2014
 *
 */

var template = require('../core.util.template'),
    marketDataService = require('../core.data.service.market'),
    formBuilder = require('../core.backend.form'),
    formidable = requireCommon('formidable'),
    uuid = requireCommon('node-uuid');

/**
 * Applications Market route
 *
 * @param utils
 */
module.exports = function (utils) {

  /* market */
  utils.router.get('/market', utils.authorizator.isAuthorized(['user']), function (req, res) {
    marketDataService.getAllApplications(null, function (err, applications) {
      var requestVariables = template.generate(req);
      requestVariables.applications = applications;
      res.render('backend/market/market.html', requestVariables);
    });
  });

  /* applications */
  utils.router.get('/market/applications/list?', utils.authorizator.isAuthorized(['user']), function (req, res) {
    var tag = req.sanitize('tag').toString();
    // TODO ASSESS security - possible injection
    var criteria;
    if (tag) {
      criteria = {
        tags: {
          $elemMatch: {
            $eq: tag
          }
        }
      };
    }
    marketDataService.getAllApplications(criteria, function (err, applications) {
      var templateVariables = template.generate(req);
      templateVariables.apps = applications;
      templateVariables.tag = tag;
      res.render('backend/market/list-applications.html', templateVariables);
    });
  });

  /* create new application */
  utils.router.get('/market/applications/new', utils.authorizator.isAuthorized(['admin', 'provider']), function (req, res) {
    var requestVariables = template.generate(req);
    var formData = {
      id: uuid.v4(), // new id for each app
      owner: req._passport.session.user // current user id
    };
    requestVariables.form = formBuilder.buildLocal("application.json", formData, '/market/applications/save');
    res.render('backend/market/add-application.html', requestVariables);
  });

  /* edit application */
  utils.router.get('/market/applications/update/:id', utils.authorizator.isAuthorized(['admin', 'provider']), function (req, res) {
    var id = req.sanitize('id').toString();
    // TODO ASSESS security - possible injection
    marketDataService.getApplication({id: id}, function (err, application) {
      if (application) {
        var requestVariables = template.generate(req);
        requestVariables.form = formBuilder.buildLocal("application.json", application, '/market/applications/save');
        res.render('backend/market/add-application.html', requestVariables);
      } else {
        var options = {
          message: "",
          request: req
        };
        options.message = utils.notifierHelper.INFO.MARKET_APPLICATION_INVALID();
        utils.notifier.notify(options, utils.notifierHelper.handleError);
        // redirect to the origin url
        res.redirect(req.header('Referer'));
      }
    });
  });

  /* remove application */
  utils.router.get('/market/applications/delete/:id', utils.authorizator.isAuthorized(['admin', 'provider']), function (req, res) {
    var id = req.sanitize('id').toString();
    // TODO ASSESS security - possible injection
    marketDataService.getApplication({id: id}, function (err, application) {
      // mandatory notifier POPUP options
      var options = {
        message: "",
        request: req
      };
      if (application) {
        // TODO delete only if the user is admin or the owner of the application
        marketDataService.deleteApplication(application, function () {
        });
        options.message = utils.notifierHelper.INFO.MARKET_APPLICATION_DELETION();
      } else {
        options.message = utils.notifierHelper.ERROR.MARKET_APPLICATION_DELETION();
      }
      utils.notifier.notify(options, utils.notifierHelper.handleError);
      // redirect to the origin url
      res.redirect(req.header('Referer'));
    });
  });

  /* add or update application */
  utils.router.post('/market/applications/save', utils.authorizator.isAuthorized(['admin', 'provider']), function (req, res) {
    // mandatory notifier POPUP options
    var options = {
      notification: "",
      request: req
    };
    // TODO ASSESS security - possible injection
    marketDataService.createOrUpdateApplication(req.body, function (err) {
      if (err) {
        options.notification = utils.notifierHelper.ERROR.MARKET_APPLICATION_CREATION(req.user);
      } else {
        options.notification = utils.notifierHelper.SUCCESS.MARKET_APPLICATION_CREATION(req.user);
      }
      utils.notifier.notify(options, utils.notifierHelper.handleError);
    });
    options.notification = utils.notifierHelper.INFO.MARKET_APPLICATION_CREATION(req.user);
    options.notificationSystems = ['DATABASE'];
    utils.notifier.notify(options, utils.notifierHelper.handleError);
    res.redirect('/market');
  });

  /* add or update application */
  utils.router.post('/market/applications/upload', utils.authorizator.isAuthorized(['admin', 'provider']), function (req, res) {
    //TODO add options to configurations
    var form = new formidable.IncomingForm({
      uploadDir: "/tmp",
      keepExtensions: true,
      maxFieldsSize: 200 * 1024 * 1024,
      maxFields: 1
    });
    form.on('fileBegin', function (name, file) {
      utils.logger.log("debug", "receiving file path json " + name + " - " + JSON.stringify(file));
    });
    form.parse(req, function (err, fields, files) {
      if (err) {
        utils.logger.log("error", "uploading files: " + JSON.stringify(files) + ". " + err);
        //res.status(500).send();
      } else {
        utils.logger.log("info", "uploaded files: " + JSON.stringify(files));
        //res.status(200).send();
        // TODO handle uploaded files
      }
    });
  });

};