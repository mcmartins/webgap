/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 05-05-2014
 *
 */

var name, email, language, photo;

function UserPojo(user) {
    // always initialize all instance properties
    this.name = user.name;
    this.email = user.email;
    this.photo = user.photo;
    this.language = user.language;
}

UserPojo.prototype.getName = function () {
    return this.name;
};

UserPojo.prototype.setName = function (name) {
    return this.name = name;
};

UserPojo.prototype.getEmail = function () {
    return this.email;
};

UserPojo.prototype.setEmail = function (email) {
    return this.email = email;
};

UserPojo.prototype.getLanguage = function () {
    return this.language;
};

UserPojo.prototype.setLanguage = function (language) {
    this.language = language;
};

UserPojo.prototype.getPhoto = function () {
    return this.photo;
};

UserPojo.prototype.setPhoto = function (photo) {
    this.photo = photo;
};

module.exports = UserPojo;