/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 03-05-2014
 *
 */

var authRouter = require('../core.route.auth'),
    backendRouter = require('../core.route.backend'),
    landingRouter = require('../core.route.landing'),
    notificationRouter = require('../core.route.notification'),
    template = require('../core.util.template');

module.exports = function (utils) {

    utils.logger.log("info", "initializing Routes...");

    /* login */
    authRouter.init(utils);

    /* account */
    backendRouter.init(utils);

    /* landing */
    landingRouter.init(utils);

    /* notifications */
    notificationRouter.init(utils);

    /* index */
    utils.router.get('/', function (req, res) {
        res.render('index.html', template.generate(req));
    });

    /* 500 server error */
    utils.router.get('/failure', function (req, res) {
        res.status(500).render('common/500.html', template.generate(req));
    });

    /* 400 unauthorized error */
    utils.router.get('/bad-request', function (req, res) {
        res.status(400).render('common/400.html', template.generate(req));
    });

    /* 401 unauthorized error */
    utils.router.get('/unauthorized', function (req, res) {
        res.status(401).render('common/401.html', template.generate(req));
    });

    /* 404 not found error */
    utils.router.get('/not-found', function (req, res) {
        res.status(404).render('common/404.html', template.generate(req));
    });

    /* 408 timeout */
    utils.router.get('/timeout', function (req, res) {
        res.status(408).render('common/408.html', template.generate(req));
    });

};