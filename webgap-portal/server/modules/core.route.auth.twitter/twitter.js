/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 23-05-2014
 *
 */

var twitterStrategy = requireCommon('passport-twitter').Strategy;

module.exports = function (utils) {

    var strategy = new twitterStrategy({
            consumerKey: utils.configuration.get('MODULE.AUTHENTICATION.TWITTER.CLIENT_ID'),
            consumerSecret: utils.configuration.get('MODULE.AUTHENTICATION.TWITTER.CLIENT_SECRET'),
            callbackURL: [utils.configuration.get('SERVER.PROTOCOL'), utils.configuration.get('SERVER.HOST'), ':',
                utils.configuration.get('SERVER.APACHE_PORT'), '/auth/twitter/oauth2callback'].join(''),
            passReqToCallback: true
        }, function (req, accessToken, refreshToken, profile, done) {
            process.nextTick(function () {
                profile.accessToken = accessToken || null;
                profile.tokenType = null;
                profile.expiresIn = null;
                profile.refreshToken = refreshToken || null;
                profile.loggedUser = req.user || null;
                return done(null, profile);
            });
        }
    );

    utils.passport.use(strategy);

    utils.router.get('/auth/twitter', utils.passport.authenticate('twitter'));

    utils.router.get('/auth/twitter/oauth2callback', utils.passport.authenticate('twitter', {failureRedirect: '/'}),
        function (req, res) {
            res.redirect('/account/profile');
        });

};