/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 07-08-2014
 *
 */

var S = requireCommon('string');

/**
 * Template utility methods
 */
module.exports = function (utils) {

    utils.ejs.filters.i18n = function (key) {
        return Array.isArray(key) ? utils.i18n.__(key[0], key[1]) : utils.i18n.__(key);
    };

    utils.ejs.filters.truncate = function (value) {
        return Array.isArray(value) ? S(value[0]).truncate(value[1]).s : S(value).truncate(15).s;
    };

    utils.ejs.filters.ND = function (val) {
        return val === undefined || val === null ? utils.i18n.t('general.not-defined') : val;
    };

    utils.ejs.filters.NA = function (val) {
        return val === undefined || val === null ? utils.i18n.t('general.not-available') : val;
    };

    utils.ejs.filters.formatTimeFromNow = function (val) {
        return utils.formatter.formatDateFromNow(val);
    };

    utils.ejs.filters.formatBytes = function (val) {
        return utils.formatter.formatBytes(val);
    };
};