/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 07-08-2014
 *
 */

var Notification = requireCommon('common.notifier').Notifier.Notification;

function NotifierHelper(utils) {
  var self = this;
  // this eliminates the need to use 'new' keyword when instantiating the module
  if (!(self instanceof NotifierHelper)) {
    return new NotifierHelper();
  }
  if (!utils && !utils.notifier) {
    throw new Error('Missing mandatory parameter!');
  }
  // handle info messages
  this.INFO = {
    /**
     * @return {boolean}
     */
    MARKET_APPLICATION_CREATION: function (user, data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.INFO,
        messageTitle: 'messages.info.market-application-creation-title',
        messageText: 'messages.info.market-application-creation',
        messageData: data
      });
      var action = new Notification.Action();
      return new Notification(user.id, message, action, null);
    },
    MARKET_APPLICATION_DELETION: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.INFO,
        messageTitle: 'messages.info.market-application-deletion-title',
        messageText: 'messages.info.market-application-deletion',
        messageData: data
      });
      var action = new Notification.Action();
      return new Notification(user.id, message, action, null);
    }
  };
  // handle success messages
  this.SUCCESS = {
    AUTH_REVOKE_ACCESS: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.SUCCESS,
        messageTitle: 'messages.success.auth-revoke-access-title',
        messageText: 'messages.success.auth-revoke-access',
        messageData: data
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.DONE;
      return new Notification(user.id, message, action, null);
    },
    MARKET_APPLICATION_CREATION: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.SUCCESS,
        messageTitle: 'messages.success.market-application-creation-title',
        messageText: 'messages.success.market-application-creation',
        messageData: data
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.DONE;
      return new Notification(user.id, message, action, null);
    },
    MARKET_APPLICATION_DELETION: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.SUCCESS,
        messageTitle: 'messages.success.market-application-deletion-title',
        messageText: 'messages.success.market-application-deletion',
        messageData: data
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.DONE;
      return new Notification(user.id, message, action, null);
    },
    ACCOUNT_APPLICATION_ADD: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.SUCCESS,
        messageTitle: 'messages.success.account-application-add-title',
        messageText: 'messages.success.account-application-add',
        messageData: data
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.DONE;
      return new Notification(user.id, message, action, null);
    },
    ACCOUNT_APPLICATION_REMOVE: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.SUCCESS,
        messageTitle: 'messages.success.account-application-remove-title',
        messageText: 'messages.success.account-application-remove',
        messageData: data
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.DONE;
      return new Notification(user.id, message, action, null);
    }
  };
  // handle warning messages
  this.WARNING = {
    MARKET_APPLICATION_CREATION: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.WARNING,
        messageTitle: 'messages.warning.market-application-creation-title',
        messageText: 'messages.warning.market-application-creation',
        messageData: data
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.DONE;
      return new Notification(user.id, message, action, null);
    },
    MARKET_APPLICATION_DELETION: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.WARNING,
        messageTitle: 'messages.warning.market-application-deletion-title',
        messageText: 'messages.warning.market-application-deletion',
        messageData: data
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.DONE;
      return new Notification(user.id, message, action, null);
    }
  };
  // handle error messages
  this.ERROR = {
    AUTH_REVOKE_ACCESS_GENERIC: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.ERROR,
        messageTitle: 'messages.error.auth-revoke-access-title',
        messageText: 'messages.error.auth-revoke-access',
        messageData: data
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.FAILED;
      return new Notification(user.id, message, action, null);
    },
    AUTH_REVOKE_ACCESS_MISSING_PARAM: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.ERROR,
        messageTitle: 'messages.error.auth-revoke-access-missing-param-title',
        messageText: 'messages.error.auth-revoke-access-missing-param',
        messageData: data
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.FAILED;
      return new Notification(user.id, message, action, null);
    },
    MARKET_APPLICATION_INVALID: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.ERROR,
        messageTitle: 'messages.error.market-application-invalid-title',
        messageText: 'messages.error.market-application-invalid',
        messageData: data
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.FAILED;
      return new Notification(user.id, message, action, null);
    },
    MARKET_APPLICATION_CREATION: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.ERROR,
        messageTitle: 'messages.error.market-application-creation-title',
        messageText: 'messages.error.market-application-creation',
        messageData: data
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.FAILED;
      return new Notification(user.id, message, action, null);
    },
    MARKET_APPLICATION_DELETION: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.ERROR,
        messageTitle: 'messages.error.market-application-deletion-title',
        messageText: 'messages.error.market-application-deletion',
        messageData: data
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.FAILED;
      return new Notification(user.id, message, action, null);
    },
    ACCOUNT_APPLICATION_ADD: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.ERROR,
        messageTitle: 'messages.error.account-application-add-title',
        messageText: 'messages.error.account-application-add',
        messageData: data
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.FAILED;
      return new Notification(user.id, message, action, null);
    },
    ACCOUNT_APPLICATION_REMOVE: function (data) {
      var message = new Notification.Message({
        messageType: Notification.Message.MessageType.ERROR,
        messageTitle: 'messages.error.account-application-remove-title',
        messageText: 'messages.error.account-application-remove',
        messageData: data
      });
      var action = new Notification.Action();
      action.status = Notification.Action.Status.FAILED;
      return new Notification(user.id, message, action, null);
    }
  };
}

NotifierHelper.prototype.register = function () {
};

NotifierHelper.prototype.handleError = function (err) {
  if (err) {
    this.logger.error('Something went wrong with the notification: \n' + err);
  }
};

module.exports.NotifierHelper = NotifierHelper;