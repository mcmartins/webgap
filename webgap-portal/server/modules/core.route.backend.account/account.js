/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

var userDataService = require('../core.data.service.user'),
    UserPojo = requireCommon('common.data.schema').UserPojo,
    formBuilder = require('../core.backend.form'),
    template = require('../core.util.template');

module.exports = function (utils) {

    /* profile */
    utils.router.get('/account/profile', utils.authorizator.isAuthorized(['user']), function (req, res) {
        userDataService.getApplications(req.user, function (err, applications) {
            var requestVariables = template.generate(req);
            requestVariables.applications = applications;
            res.render('backend/account/profile.html', requestVariables);
        });
    });

    /* terminal */
    utils.router.get('/account/terminal', utils.authorizator.isAuthorized(['user']), function (req, res) {
        res.render('backend/services/terminal.html', template.generate(req));
    });

    /* settings */
    utils.router.get('/account/settings', utils.authorizator.isAuthorized(['user']), function (req, res) {
        var requestVariables = template.generate(req);
        requestVariables.providers = template.convertCloudProviders(req);
        requestVariables.form =
            formBuilder.buildLocal("user.json", new UserPojo(req.user), '/account/settings/save');
        res.render('backend/account/settings.html', requestVariables);
    });

    /* settings */
    utils.router.get('/account/settings/save', utils.authorizator.isAuthorized(['user']), function (req, res) {
        var requestVariables = template.generate(req);
        requestVariables.providers = template.convertCloudProviders(req);
        requestVariables.form =
            formBuilder.buildLocal("user.json", new UserPojo(req.user), '/account/settings/save');
        res.render('backend/account/settings.html', requestVariables);
    });

    /* add app */
    utils.router.get('/account/applications/add/:id', utils.authorizator.isAuthorized(['user']), function (req, res) {
        var id = req.sanitize('id').toString();
        // TODO ASSESS security - possible injection
        userDataService.addApplication(req.user, id, function (err, result) {
            // mandatory notifier POPUP options
            var options = {
                message: "",
                request: req
            };
            if (err) {
                utils.logger.error(err);
                options.message = utils.notifierHelper.ERROR.ACCOUNT_APPLICATION_ADD({name: err.name});
            } else {
                options.message = utils.notifierHelper.SUCCESS.ACCOUNT_APPLICATION_ADD({name: result});
            }
            utils.notifier.notify(options, utils.notifierHelper.handleError);
            // redirect to the origin url
            res.redirect(req.header('Referer'));
        });
    });

    /* remove app */
    utils.router.get('/account/applications/remove/:id', utils.authorizator.isAuthorized(['user']), function (req, res) {
        var id = req.sanitize('id').toString();
        // TODO ASSESS security - possible injection
        userDataService.removeApplication(req.user, id, function (err) {
            // mandatory notifier POPUP options
            var options = {
                message: "",
                request: req
            };
            if (err) {
                utils.logger.error(err);
                options.message = utils.notifierHelper.ERROR.ACCOUNT_APPLICATION_REMOVE();
            } else {
                options.message = utils.notifierHelper.SUCCESS.ACCOUNT_APPLICATION_REMOVE();
            }
            utils.notifier.notify(options, utils.notifierHelper.handleError);
            // redirect to the origin url
            res.redirect(req.header('Referer'));
        });
    });

    /* workspace */
    utils.router.get('/account/workspace', utils.authorizator.isAuthorized(['user']), function (req, res) {
        res.render('backend/services/workspace.html', template.generate(req));
    });

    /* ide */
    utils.router.get('/account/ide', utils.authorizator.isAuthorized(['user']), function (req, res) {
        res.render('backend/services/ide.html', template.generate(req));
    });

    /* hardcoded links */
    utils.router.get('/service/sync/:service', utils.authorizator.isAuthorized(['user']), function (req, res) {
        var service = req.sanitize('service').toString();
        var templateVariables = template.generate(req);
        templateVariables.terminalUrl = "https://instance01.webgap.eu/service/" + service;
        res.render('backend/services/terminal.html', templateVariables);
    });

    /* async service */
    utils.router.get('/account/service/async', utils.authorizator.isAuthorized(['user']), function (req, res) {
        // TODO implement async services
    });

    /* sync service */
    utils.router.get('/account/service/sync', utils.authorizator.isAuthorized(['user']), function (req, res) {
        // FIXME re-implement sync services using amqp
        var templateVariables = template.generate(req);
        var request = utils.message.Server.Request();
        request.data = utils.tokenizer.HS512.encode(JSON.stringify(req.user.account));

        utils.client.publish(utils.configuration.get('AMQP.CHANNELS.GAP'), JSON.stringify(request));
        utils.client.subscribe(request.replyToChannel);
        utils.client.on("message", function (topic, message) {
            if (topic == request.replyToChannel) {
                message = JSON.parse(message);
                utils.json.isValid(message, function (err) {
                    if (!err && utils.tokenizer.HS512.isValid(message.token)) {
                        if (message.status == utils.message.Server.Status.OK) {
                            templateVariables.gapUrl = message.result.url;
                        } else {
                            templateVariables.gapUrl = "/failure";
                            templateVariables.message = utils.message.Client.generate(utils.message.Client.Type.ERROR,
                                'error.' + message.error.code, message.error.details);
                        }
                        res.render('backend/account/terminal.html', templateVariables);
                    }
                });
            }
        });

        // define the timeout for the request
        // ensures that the response does not stall waiting for nodes to reply
        setTimeout(function () {
            // ignore timeout if resolution is finished
            if (!templateVariables.gapUrl) {
                res.statusCode = 408;
                req._passport.session.message = utils.message.Client.generate(utils.message.Client.Type.ERROR,
                    "messages.error.timeout-request-title", "messages.error.timeout-request");
                res.redirect('/timeout');
            }
            utils.client.unsubscribe(request.replyToChannel);
        }, utils.configuration.get('GENERAL.TIMEOUT.OPTIMIST'));
    });

    /* status */
    utils.router.get('/server/status', utils.authorizator.isAuthorized(['user']), function (req, res) {
        // TODO implement status page
        res.render('backend/status/status.html', template.generate(req));
    });

};