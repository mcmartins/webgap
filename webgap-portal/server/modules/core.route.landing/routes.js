/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 03-05-2014
 *
 */

var template = require('../core.util.template');

module.exports = {

    init: function (utils) {

        /* disclaimer */
        utils.router.get('/disclaimer', function (req, res) {
            res.render('landing/doc/disclaimer.html', template.generate(req));
        });

        /* terms */
        utils.router.get('/terms', function (req, res) {
            res.render('landing/doc/terms.html', template.generate(req));
        });

        /* contact */
        utils.router.get('/contact', function (req, res) {
            res.render('landing/contact.html', template.generate(req));
        });

        /* about */
        utils.router.get('/faq', function (req, res) {
            res.render('landing/faq.html', template.generate(req));
        });

        /* features */
        utils.router.get('/features', function (req, res) {
            res.render('landing/features.html', template.generate(req));
        });

    }

};