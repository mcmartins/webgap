/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

'use strict';

var entities = requireCommon('common.data.schema'),
    async = requireCommon('async'),
    configuration = requireCommon('common.configuration').load(),
    DatabaseDAO = requireCommon('common.database.dao').DatabaseDAO,
    notificationDAO = new DatabaseDAO({
      collectionName: 'notification',
      connectionString: configuration.get('DB.CONNECTION_STRING')
    });

module.exports = {

  /**
   * This method handles the notification creation.
   *
   * @param notification
   * @param callback
   */
  createNotification: function (notification, callback) {
    var self = this;
    async.waterfall([
      // load notification client DAO
      function (next) {
        self.getNotificationDAOClient(next);
      },
      // handle notification load from database
      function (notificationDAOClient) {
        notificationDAOClient.insert(notification, callback)
      }
    ]);
  },

  /**
   * This method handles the notification creation.
   *
   * @param user
   * @param id
   * @param callback
   */
  getNotification: function (user, id, callback) {
    var self = this;
    async.waterfall([
      // load notification client DAO
      function (next) {
        self.getNotificationDAOClient(next);
      },
      // handle notification load from database
      function (notificationDAOClient) {
        notificationDAOClient.findOne({
          userId: user.id,
          id: id
        }, callback);
      }
    ]);
  },

  /**
   * This method handles the notification creation.
   *
   * @param user
   * @param callback
   */
  getUserUnreadNotifications: function (user, callback) {
    var self = this;
    async.waterfall([
      // load notification client DAO
      function (next) {
        self.getNotificationDAOClient(next);
      },
      // handle notification load from database
      function (notificationDAOClient) {
        notificationDAOClient.find({
          userId: user.id,
          read: false
        }, callback);
      }
    ]);
  },

  /**
   * This method handles the notification creation.
   *
   * @param user
   * @param callback
   */
  getUserNotifications: function (user, callback) {
    var self = this;
    async.waterfall([
      // load notification client DAO
      function (next) {
        self.getNotificationDAOClient(next);
      },
      // handle notification load from database
      function (notificationDAOClient) {
        notificationDAOClient.find({
          userId: user.id
        }, callback);
      }
    ]);
  },

  /**
   * This method handles the notification creation.
   *
   * @param user
   * @param id
   * @param callback
   */
  removeNotification: function (user, id, callback) {
    var self = this;
    async.waterfall([
      // load notification client DAO
      function (next) {
        self.getNotificationDAOClient(next);
      },
      // handle notification load from database
      function (notificationDAOClient) {
        notificationDAOClient.remove({
          userId: user.id,
          id: id
        }, callback);
      }
    ]);
  },

  /**
   * This method handles the notification creation.
   *
   * @param user
   * @param id
   * @param callback
   */
  markNotificationRead: function (user, id, callback) {
    var self = this;
    async.waterfall([
      // load notification client DAO
      function (next) {
        self.getNotificationDAOClient(next);
      },
      // handle notification load from database
      function (notificationDAOClient) {
        notificationDAOClient.findAndModify({userId: user.id, id: id}, {read: true}, callback);
      }
    ]);
  },

  /**
   * Returns the DAO Client
   *
   * @param callback
   * @returns {*}
   */
  getNotificationDAOClient: function (callback) {
    notificationDAO.getClient(callback);
  }
};
