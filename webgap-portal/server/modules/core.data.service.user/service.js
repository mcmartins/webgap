/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 04-05-2014
 *
 */

'use strict';

var marketService = require('../core.data.service.market'),
    entities = requireCommon('common.data.schema'),
    async = requireCommon('async'),
    ObjectUtils = requireCommon('common.util.object').ObjectUtils,
    objectUtils = new ObjectUtils(),
    configuration = requireCommon('common.configuration').load(),
    DatabaseDAO = requireCommon('common.database.dao').DatabaseDAO,
    userDAO = new DatabaseDAO({
        collectionName: 'user',
        connectionString: configuration.get('DB.CONNECTION_STRING')
    });

module.exports = {

    /**
     * This method handles the user creation by checking:
     * if the user is logged in should create/update the identity
     * create the user otherwise
     *
     * @param profile
     * @param callback
     */
    createOrUpdateUser: function (profile, callback) {
        var self = this;
        // TODO add audit columns to user object
        async.waterfall([
            // load user client DAO
            function (next) {
                self.getUserDAOClient(next);
            },
            // handle user load from database
            function (userDAOClient, next) {
                // generate identity
                var identity = new entities.Identity(profile.id, profile.provider,
                    new entities.IdentityToken(profile.accessToken, profile.tokenType, profile.expiresIn,
                        profile.refreshToken));
                var loggedUser = profile.loggedUser;
                // TODO performance improvement - load
                async.waterfall([
                    // load user from database
                    function loggedDbUserByEmail(nextInternal) {
                        // check if there is a user registered with the main email for the current profile
                        userDAOClient.findOne({
                            email: loggedUser ? loggedUser.email : null
                        }, function (err, user) {
                            if (err) {
                                return callback(err);
                            }
                            if (user) {
                                return next(null, userDAOClient, identity, user);
                            } else {
                                return nextInternal(null);
                            }
                        });
                    },
                    function dbUserByEmail(nextInternal) {
                        // check if there is a user registered with the main email for the current profile
                        userDAOClient.findOne({
                            email: profile.emails ? profile.emails[0].value : null
                        }, function (err, user) {
                            if (err) {
                                return callback(err);
                            }
                            if (user) {
                                return next(null, userDAOClient, identity, user);
                            } else {
                                return nextInternal(null);
                            }
                        });
                    },
                    function dbUserByIdentity() {
                        // check if there is a user registered with the main email for the current profile
                        userDAOClient.findOne({
                            identities: {
                                $elemMatch: {
                                    id: identity.id,
                                    provider: identity.provider
                                }
                            }
                        }, function (err, user) {
                            if (err) {
                                return callback(err);
                            }
                            if (user) {
                                return next(null, userDAOClient, identity, user);
                            } else {
                                return next(null, userDAOClient, identity, undefined);
                            }
                        });
                    }
                ]);
            },
            // check if user is logged in and if the identity has to be added or updated
            function (userDAOClient, identity, dbUser, next) {
                // if the identity is associated update the user identity
                var index;
                // check the index of the identity if it exists
                if (dbUser) {
                    index = objectUtils.Array.indexOf(dbUser.identities, 'id', identity.id);
                    if (index >= 0) {
                        dbUser.identities.splice(index, 1);
                        dbUser.identities.push(identity);
                        userDAOClient.update({id: dbUser.id}, dbUser, function (err) {
                            // return error
                            if (err) {
                                return callback(err);
                            }
                        });
                        return callback(null, dbUser.id);
                        // if there is a logged user and the identity is not present update the user
                    } else {
                        dbUser.identities.push(identity);
                        userDAOClient.update({id: dbUser.id}, dbUser, function (err) {
                            // return error
                            if (err) {
                                return callback(err);
                            }
                        });
                        return callback(null, dbUser.id);
                    }
                } else {
                    return next(null, userDAOClient, identity);
                }
            },
            // seems the user is not registered, create and persist it
            function (userDAOClient, identity) {
                var user = new entities.User(profile.displayName);
                user.setEmail(profile.emails ? profile.emails[0].value : null);
                user.setPhoto(profile.photos ? profile.photos[0].value : null);
                user.addIdentity(identity);
                userDAOClient.save(user, function (err) {
                    // return error
                    if (err) {
                        return callback(err);
                    }
                });
                return callback(null, user.id);
            }
        ]);
    },

    /**
     * Revoke access to provider
     *
     * @param user the user in session
     * @param provider the provider to revoke access
     * @param callback
     */
    revokeProvider: function (user, provider, callback) {
        var self = this;
        async.waterfall([
            // load user client DAO
            function (next) {
                self.getUserDAOClient(next);
            },
            // load user
            function (userDAOClient, next) {
                var index;
                // check if there is a user registered with the main email for the current profile
                async.waterfall([
                    // load user from database
                    function (initializer) {
                        userDAOClient.findOne({
                            email: user ? user.email : null
                        }, initializer);
                    },
                    function (dbUser) {
                        // check the index of the identity if it exists
                        if (dbUser) {
                            index = objectUtils.Array.indexOf(dbUser.identities, 'provider', provider);
                        }
                        // proceed
                        return next(null, userDAOClient, dbUser, index);
                    }
                ]);
            },
            // check if user is logged in and if the identity has to be added or updated
            function (userDAOClient, dbUser, index) {
                if (index >= 0) {
                    dbUser.identities.splice(index, 1);
                    userDAOClient.update({id: dbUser.id}, dbUser, function (err) {
                        // return error
                        if (err) {
                            return callback(err);
                        }
                    });
                    return callback();
                } else {
                    return callback(new Error("The provider is not associated to the user!"));
                }
            }
        ]);
    },

    /**
     * Subscribe access to application
     *
     * @param user the user in session
     * @param appId the application ID
     * @param callback
     */
    addApplication: function (user, appId, callback) {
        var self = this;
        async.waterfall([
            // load user client DAO
            function (next) {
                self.getUserDAOClient(next);
            },
            function (userDAOClient, next) {
                async.parallel({
                    // load user from database
                    user: function (initializer) {
                        userDAOClient.findOne({id: user.id}, function (err, user) {
                            if (err) {
                                return callback(err);
                            }
                            if (user) {
                                return initializer(null, user);
                            } else {
                                return callback(new Error('Invalid User!'));
                            }
                        });
                    },
                    // load application from database
                    application: function (initializer) {
                        marketService.getApplication({id: appId}, function (err, application) {
                            if (err) {
                                return callback(err);
                            }
                            if (application) {
                                return initializer(null, application);
                            } else {
                                return callback(new Error('Invalid Application!' + appId));
                            }
                        });
                    }
                }, function initializer(err, results) {
                    if (err) {
                        return callback(err);
                    }
                    return next(null, userDAOClient, results.user, results.application);
                });
            },
            function (userDAOClient, user, application) {
                // check if user can have more apps
                if (user.applications.length > configuration.get('MODULE.ACCOUNT.USER_MAX_APPS')) {
                    return callback(new Error('User cannot associate more applications!'));
                }
                // check if user has already access to the application
                async.waterfall([
                    function (next) {
                        if (user.applications.some(function (applicationId) {
                                return applicationId === application.id;
                            })) {
                            var error = new Error('Application ' + application.name + ' is already associated with the user!');
                            error.name = application.name;
                            return callback(error);
                        } else {
                            return next();
                        }
                    },
                    function handle() {
                        // add application and update user
                        user.applications.push(application.id);
                        userDAOClient.update({id: user.id}, user, function (err) {
                            if (err) {
                                throw err;
                            }
                        });
                        return callback(null, application.name);
                    }
                ]);
            }
        ]);
    },

    /**
     * Remove application subscription
     *
     * @param user the user in session
     * @param appId application ID
     * @param callback
     */
    removeApplication: function (user, appId, callback) {
        var self = this;
        async.waterfall([
            // load user client DAO
            function (next) {
                self.getUserDAOClient(next);
            },
            function (userDAOClient, next) {
                async.parallel({
                    // load user from database
                    user: function (initializer) {
                        userDAOClient.findOne({id: user.id}, function (err, user) {
                            if (err) {
                                return callback(err);
                            }
                            if (user) {
                                return initializer(null, user);
                            } else {
                                return callback(new Error('Invalid User!'));
                            }
                        });
                    },
                    // load application from database
                    application: function (initializer) {
                        marketService.getApplication({id: appId}, function (err, application) {
                            if (err) {
                                return callback(err);
                            }
                            if (application) {
                                return initializer(null, application);
                            } else {
                                return callback(new Error('Invalid Application!' + appId));
                            }
                        });
                    }
                }, function initializer(err, results) {
                    if (err) {
                        return callback(err);
                    }
                    return next(null, userDAOClient, results.user, results.application);
                });
            },
            function (userDAOClient, user, application) {
                // check if user has access to the application
                async.detect(user.applications,
                    function (applicationId, handle) {
                        if (applicationId === application.id) {
                            return handle(application);
                        }
                    },
                    function handle(result) {
                        if (!result) {
                            return callback(new Error('Application is not associated with the user!' + application.name));
                        }
                        var index = user.applications.indexOf(application.id);
                        // no risk because we check that the user has access to the application before
                        user.applications.splice(index, 1);
                        userDAOClient.update({id: user.id}, user, function (err) {
                            if (err) {
                                throw err;
                            }
                        });
                        return callback();
                    }
                );
            }
        ]);
    },

    getApplications: function (user, callback) {
        var applications = [];
        async.each(user.applications,
            function (appId, callback) {
                marketService.getApplication({id: appId}, function (err, application) {
                    if (err) {
                        callback(err);
                    }
                    applications.push(application);
                    callback();
                });
            }, function (err) {
                if (err) {
                    callback(err);
                }
                callback(null, applications);
            }
        );
    },

    /**
     * Returns the DAO Client
     *
     * @param callback
     * @returns {*}
     */
    getUserDAOClient: function (callback) {
        userDAO.getClient(callback);
    },

    /**
     * Returns a user based on his identity
     *
     * @param identity the identity
     * @param callback
     * @returns {*} the user
     */
    getUserByIdentity: function (identity, callback) {
        userDAO.getClient(function (err, client) {
            if (err) {
                return callback(err);
            }
            client.findOne({
                identities: {
                    $elemMatch: {
                        id: identity.id,
                        provider: identity.provider
                    }
                }
            }, callback);
        });
    },

    /**
     * Returns a user based on his identity
     *
     * @param id the id
     * @param callback
     * @returns {*} the user
     */
    getUserById: function (id, callback) {
        userDAO.getClient(function (err, client) {
            if (err) {
                return callback(err);
            }
            client.findOne({
                id: id
            }, callback);
        });
    },

    /**
     * Returns a user based on his email
     *
     * @param email the email
     * @param callback
     * @returns {*} the user
     */
    getUserByEmail: function (email, callback) {
        userDAO.getClient(function (err, client) {
            if (err) {
                return callback(err);
            }
            client.findOne({email: email}, callback);
        });
    },

    /**
     * Returns all users matching a criteria
     * If no criteria is provided returns all
     *
     * @param criteria the criteria
     * @param callback
     * @returns {*} the list of users
     */
    getAllUsers: function (criteria, callback) {
        userDAO.getClient(function (err, client) {
            client.find(criteria, callback);
        });
    }
};
