/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 11-05-2014
 *
 */

var ObjectUtils = requireCommon('common.util.object').ObjectUtils,
    Authorizator = requireCommon('common.util.authorizator').Authorizator,
    authorizator = new Authorizator(),
    objectUtils = new ObjectUtils();

/**
 * Template utility methods
 */
module.exports = {

    /**
     * Generates a default Object for ejs rendering
     * New properties can be added after
     *
     * @param req the session request
     * @returns {{url: (originalUrl|*), user: *, message: *}}
     */
    generate: function (req) {
        var templateVariables = {
            url: req.originalUrl,
            user: objectUtils.Object.clone(req.user),
            message: req._passport.session.message,
            authorizator: authorizator
        };
        // delete messages from session to prevent loading them on next request
        req._passport.session.message = undefined;
        return templateVariables;
    },

    /**
     * Returns a list of all the cloud providers loaded into passport
     *
     * @param req the session request
     * @returns {Array} an array with the passport strategies
     */
    convertCloudProviders: function (req) {
        var providers = [], strategies = req._passport.instance._strategies;
        for (var strategy in strategies) {
            if (strategies.hasOwnProperty(strategy)) {
                //ignore local session strategy
                if (strategy == 'session') {
                    continue;
                }
                var provider = {};
                // remove extra oauth etc info using split
                provider.name = strategy.split("-")[0];
                provider.cloudStorage = strategies[strategy].cloudStorage;
                providers.push(provider);
            }
        }
        return providers;
    }
};