/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 03-05-2014
 *
 */

var userService = require('../core.data.service.user'),
    template = require('../core.util.template');

module.exports = function (utils) {

    /* admin */
    utils.router.get('/admin', utils.authorizator.isAuthorized(['admin']), function (req, res) {
        res.render('backend/admin/admin.html', template.generate(req));
    });

    /* users */
    utils.router.get('/admin/users', utils.authorizator.isAuthorized(['admin']), function (req, res) {
        var variables = template.generate(req);
        variables.users = userService.getAllUsers();
        res.render('backend/admin/users.html', variables);
    });

    /* stats */
    utils.router.get('/admin/stats', utils.authorizator.isAuthorized(['admin']), function (req, res) {
        var variables = template.generate(req);
        variables.stats = [];
        var request = utils.message.Server.Request();
        utils.client.publish(utils.configuration.get('AMQP.CHANNELS.STATS'), JSON.stringify(request));
        utils.client.subscribe(request.replyToChannel);
        utils.client.on("message", function (topic, message) {
            if (topic == request.replyToChannel) {
                message = JSON.parse(message);
                utils.json.isValid(message, function (err) {
                    if (err && utils.tokenizer.HS512.isValid(message.token)) {
                        variables.stats.push(message.result.data);
                    }
                });
            }
        });

        // define the timeout for the request
        // ensures that the response does not stall waiting for nodes to reply
        setTimeout(function () {
            utils.client.unsubscribe(request.replyToChannel);
            res.render('backend/admin/stats.html', variables);
        }, utils.configuration.get('GENERAL.TIMEOUT.OPTIMIST'));
    });

    /* accounts */
    utils.router.get('/admin/accounts', utils.authorizator.isAuthorized(['admin']), function (req, res) {
        var variables = template.generate(req);
        variables.accounts = [];
        function loadAccounts(key, val) {
            variables.accounts.push(JSON.parse(val));
        }

        //accountService.getAllAccounts(loadAccounts);
        res.render('backend/admin/accounts.html', variables);
    });

    /* configurations */
    utils.router.get('/admin/configurations', utils.authorizator.isAuthorized(['admin']), function (req, res) {
        var variables = template.generate(req);
        variables.configurations = JSON.stringify(utils.configuration);
        res.render('backend/admin/configurations.html', variables);
    });

    /* maintenance */
    utils.router.get('/admin/maintenance', utils.authorizator.isAuthorized(['admin']), function (req, res) {
        res.render('backend/admin/maintenance.html', template.generate(req));
    });

    /* tools */
    utils.router.get('/admin/tools', utils.authorizator.isAuthorized(['admin']), function (req, res) {
        res.render('backend/admin/tools.html', template.generate(req));
    });

};