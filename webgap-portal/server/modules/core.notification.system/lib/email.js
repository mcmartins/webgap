/**
 * (C) Copyright 2014 WebGAP (http://www.webgap.eu/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created by: ManuelMartins
 * Created on: 05-03-2015
 *
 */

var util = requireCommon('util'),
    nodeMailer = requireCommon('nodemailer');

function EmailSystem(utils) {
    var self = this;
    utils = utils || {};
    // this eliminates the need to use 'new' keyword when instantiating the module
    if (!(self instanceof EmailSystem)) {
        return new EmailSystem(utils);
    }
    self.configuration = utils.configuration;
    self.AbstractNotificationSystem = utils.notifier.AbstractNotificationSystem;
}

EmailSystem.prototype.notify = function (options, callback) {
    var mailOpts, smtpTransport, self = this;

    smtpTransport = nodeMailer.createTransport('SMTP', {
        service: self.configuration.get(''),
        auth: {
            user: self.configuration.get(''),
            pass: self.configuration.get('')
        }
    });

    mailOpts = {
        from: options.from,
        replyTo: options.from,
        to: options.to,
        subject: options.subject,
        html: options.body
    };

    smtpTransport.sendMail(mailOpts, function (error, response) {
        if (error) {
            console.log(error);
        } else {
            console.log('Message sent: ' + response.message);
        }
        smtpTransport.close();
    });

};

module.exports.EmailSystem = EmailSystem;