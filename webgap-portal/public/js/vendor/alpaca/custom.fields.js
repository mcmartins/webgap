$.alpaca.Fields.UserField = $.alpaca.Fields.SelectField.extend({
    // TODO check country field on implementation
    // shall load the list of users from database
    /**
     * @see Alpaca.Field#getFieldType
     */
    getFieldType: function () {
        return "user";
    },

    /**
     * @see Alpaca.Fields.Field#setup
     */
    setup: function () {

        this.schema["enum"] = [];
        this.options.optionLabels = [];

        // TODO load users by invoking an url using an authentication token
        // the same approach can be used to load anything form the system to create custom fields / other components
        var objectMap = this.getMessage("countries");
        if (objectMap) {
            for (var countryKey in objectMap) {
                this.schema["enum"].push(countryKey);
                this.options.optionLabels.push(objectMap[countryKey]);
            }
        }

        this.base();
    }

    /* builder_helpers */
    ,

    /**
     * @see Alpaca.Fields.TextField#getTitle
     */
    getTitle: function () {
        return "User Field";
    },

    /**
     * @see Alpaca.Fields.TextField#getDescription
     */
    getDescription: function () {
        return "Provides a dropdown selector of all users available in the system.";
    }

});

Alpaca.registerFieldClass("user", Alpaca.Fields.UserField);
Alpaca.registerDefaultFormatFieldMapping("user", "user");