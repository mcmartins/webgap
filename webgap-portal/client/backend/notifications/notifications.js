$(function () {

  // toastr notifications configuration
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "positionClass": "toast-top-center",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "500",
    "hideDuration": "1000",
    "timeOut": "10000",
    "extendedTimeOut": "10000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

  /**
   * Handles notification loading
   */
  function loadNotifications() {
    $.get('/notifications/list/unread', function (html) {
      if (html) {
        // update popup
        $('#notificationsMenu').html(html);
        // update count of messages
        updateCount();
        // add events to mark as read
        $('li.notification-not-seen').click(function markAsRead() {
          var notification = this;
          $(notification).removeClass("notification-not-seen");
          // update count of messages
          updateCount();
          $.post('/notifications/mark-read/', notification.id);
        });
      }
    });
  }

  /**
   * Handles UI updates
   */
  function updateCount() {
    var count = $('li.notification-not-seen').length;
    if (count > 0) {
      $('#notificationsCount').text(count);
    } else {
      $('#notificationsCount').text('');
    }
  }

  // load notifications every 20 seconds
  setInterval(function () {
    loadNotifications();
  }, 20000);

  // load notifications first time
  loadNotifications();
});